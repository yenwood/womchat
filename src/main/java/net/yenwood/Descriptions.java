package net.yenwood;

import java.io.File;
import java.io.IOException;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Descriptions {
	private static Configuration configuration;
	private static WoMChat plugin;

	public Descriptions(WoMChat plugin, Configuration config){
		Descriptions.configuration = config;
		Descriptions.plugin = plugin;
	}
	
	public static String get(ProxiedPlayer player){
		String uuid = player.getUniqueId().toString().replace("-", "");
		String description = configuration.getString("custom."+uuid);
		
		return (description != null)? description : "";
	}
	
	public static String get(String path){
		String description = configuration.getString(path);
		
		return (description != null)? description : "";
	}
	
	public static void set(ProxiedPlayer player, String value){
		String uuid = player.getUniqueId().toString().replace("-", "");
		configuration.set("custom."+uuid, value);
	}
	
	public void save(){
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(plugin.getDataFolder(), "descriptions.yml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
