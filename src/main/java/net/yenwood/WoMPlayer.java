package net.yenwood;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;


public class WoMPlayer {
	private ProxiedPlayer player;
	
	public WoMPlayer(ProxiedPlayer player){
		this.player = player;
	}
	
	public Server getServer(){
		return this.player.getServer();
	}

}
