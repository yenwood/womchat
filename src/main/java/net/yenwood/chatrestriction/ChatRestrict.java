package net.yenwood.chatrestriction;

public class ChatRestrict {
	private String reason;
	private Long started; // MS from Epoch
	private Long ends; // MS from Epoch
	private int messages;
	private Boolean active;
	
	private int interval; // in seconds
	
	public ChatRestrict(){
		this.started = System.currentTimeMillis();
	}
	
	public ChatRestrict setMessages(int msgs){
		this.messages = msgs;
		
		return this;
	}
	
	public ChatRestrict incrementMessages(){
		this.messages = this.messages + 1;
		
		return this;
	}
	
	public ChatRestrict decrementMessages(){
		if(this.messages - 1 < 0){
			this.messages = 0;
		}else{
			this.messages = this.messages - 1;
		}
		
		return this;
	}
	
	public ChatRestrict setExpire(Long time){
		this.ends = time;
		
		return this;
	}
	
	public ChatRestrict setReason(String reason){
		this.reason = reason;
		
		return this;
	}
	
	public ChatRestrict setActive(Boolean tf){
		this.active = tf;
		
		return this;
	}
	
	public ChatRestrict setInterval(int interval){
		this.interval = interval;
		
		return this;
	}
	
	public Boolean isActive(){
		if(this.active){
			if(this.ends <= System.currentTimeMillis()){
				this.active = false;
			}
		}
		
		return this.active;
	}
	
	public Long getStart(){
		return this.started;
	}
	
	public Long getExpire(){
		return this.ends;
	}
	
	public String getReason(){
		return this.reason;
	}
	
	public int getMessages(){
		return this.messages;
	}
	
	public int getInterval(){
		return this.interval;
	}
}
