package net.yenwood.chatrestriction;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.WoMChat;
import net.yenwood.util.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RestrictCommand extends Command {
	private WoMChat plugin;

	public RestrictCommand(WoMChat plugin) {
		super("restrict");
		this.plugin = plugin;
	}

	@SuppressWarnings("unused")
	@Override
	public void execute(CommandSender csender, String[] args) {
		ProxiedPlayer sender = (ProxiedPlayer)csender;
		TextComponent message = new TextComponent("");
		ProxiedPlayer victim;
		int duration = 0;
		int interval = 0;
		int messages = 2;
		Pattern r = Pattern.compile("([\\d]*)(h|m|s)");
		
		// EXAMPLE:
		// /restrict Yenwood 1m2s
		// Restricted for 1 minute 2 seconds, no extra messages, gets 2 messages (default)
		
		// /restrict Yenwood 1h 5m
		// Restricted for 1 hour, extra message every 5 minutes, gets 2 messages (default)
		
		// /restrict Yenwood 2h 30m 3
		// Restricted for 2 hours, extra message every 30 minutes, gets 2 messages
		
		// /restrict Yenwood 2h 3
		// Restricted for 2 hours, gets 3 messages
		
		if(!sender.hasPermission("womchat.restrict")){
			message.addExtra("You don't have permission to use this command.");
			message.setColor(ChatColor.RED);
			sender.sendMessage(message);
			return;
		}
		
		if(args.length < 2 || args.length > 4){
			message.addExtra("Incorrect number of arguments. Usage:");
			message.setColor(ChatColor.RED);
			sender.sendMessage(message);
			message = new TextComponent("");
			message.addExtra("/restrict <username> <time> [extra message interval] [# of messages]");
			message.setColor(ChatColor.RED);
			sender.sendMessage(message);
			return;
		}
		
		if(plugin.getPlayer(args[0]) != null){
			victim = plugin.getPlayer(args[0]);
		}else{
			message.addExtra("That player is not online.");
			message.setColor(ChatColor.RED);
			sender.sendMessage(message);
			return;
		}

		// DURATION
		Matcher arg1 = r.matcher(args[1]);
		while(arg1.find()){
			String unit = arg1.group(2);
			String length = arg1.group(1);

            switch (unit) {
                case "h":
                    duration += Integer.parseInt(length) * 60 * 60;
                    break;
                case "m":
                    duration += Integer.parseInt(length) * 60;
                    break;
                case "s":
                    duration += Integer.parseInt(length);
                    break;
            }
		}

		// INTERVAL
		if(args.length > 2){
			if(Util.isInteger(args[2])){
				messages = Integer.parseInt(args[2]);
			}else{
				Matcher arg2 = r.matcher(args[2]);
				while(arg2.find()){
					String unit = arg1.group(2);
					String length = arg1.group(1);

                    switch (unit) {
                        case "h":
                            interval += Integer.parseInt(length) * 60 * 60;
                            break;
                        case "m":
                            interval += Integer.parseInt(length) * 60;
                            break;
                        case "s":
                            interval += Integer.parseInt(length);
                            break;
                    }
				}
			}
		}
		
		ChatRestrict chr = new ChatRestrict()
				.setMessages(messages)
				.setExpire(System.currentTimeMillis() + (duration * 1000))
				.setActive(true);
		ChatRestrictHandler.restrict(victim, chr);
	}

}
