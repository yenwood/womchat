package net.yenwood.chatrestriction;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.yenwood.WoMChat;
import net.yenwood.util.Util;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class ChatRestrictHandler {
	private static WoMChat plugin;
	private static HashMap<ProxiedPlayer, ChatRestrict> restrictions = new HashMap<ProxiedPlayer, ChatRestrict>();
	
	public ChatRestrictHandler(WoMChat plugin){
		ChatRestrictHandler.plugin = plugin;
	}
	
	public static int getMessages(ProxiedPlayer player){
		if(restrictions.containsKey(player)){
			return restrictions.get(player).getMessages();
		}
		return 0;
	}
	
	public static Boolean isRestricted(ProxiedPlayer player){
		if(restrictions.containsKey(player)){
			return restrictions.get(player).isActive();
		}
		return false;
	}
	
	public static ChatRestrict getChatRestrict(ProxiedPlayer player){
		if(restrictions.containsKey(player)){
			return restrictions.get(player);
		}
		return null;
	}
	
	public static void restrict(ProxiedPlayer player, ChatRestrict chr){
		restrictions.put(player, chr);
		
		TextComponent message = new TextComponent("");
		message.addExtra("You have been chat restricted for "+Util.milliToReadable(chr.getStart(), chr.getExpire()));
		message.setColor(ChatColor.RED);
		player.sendMessage(message);
		message = new TextComponent("");
		message.addExtra("You can send up to "+chr.getMessages()+" messages until the chat restriction ends.");
		message.setColor(ChatColor.RED);
		player.sendMessage(message);
		if(chr.getInterval() > 0 && chr.getInterval() * 1000 < chr.getExpire() - chr.getStart() ){
			message = new TextComponent("");
			message.addExtra("You will receive a new message to send every "+Util.milliToReadable(chr.getInterval() * 1000)+".");
			message.setColor(ChatColor.RED);
			player.sendMessage(message);
		}

		ScheduledTask task = ProxyServer.getInstance().getScheduler().schedule(ChatRestrictHandler.plugin,
                () -> {
                    if(chr.getInterval()!=0){
                        chr.incrementMessages();
                        TextComponent message1 = new TextComponent("");
                        message1.addExtra("You've earned another message, for a total of "+chr.getMessages());
                        message1.setColor(ChatColor.GREEN);
                        player.sendMessage(message1);
                        message1 = new TextComponent("");
                        message1.addExtra("You'll earn another one in "+Util.milliToReadable(chr.getInterval() * 1000));
                        message1.setColor(ChatColor.GREEN);
                        player.sendMessage(message1);
                    }
                }, chr.getInterval(), chr.getInterval(), TimeUnit.SECONDS
		    );
		
		ProxyServer.getInstance().getScheduler().schedule(ChatRestrictHandler.plugin,
                () -> {
                    plugin.getProxy().getScheduler().cancel(task);
                    TextComponent message12 = new TextComponent("");
                    message12.addExtra("Your chat restriction has ended.");
                    message12.setColor(ChatColor.GREEN);
                    player.sendMessage(message12);

                    restrictions.remove(player);
                }, chr.getExpire() - chr.getStart(), TimeUnit.MILLISECONDS
		);
	}
}
