package net.yenwood;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;

public class Mailbox {
	List<Mail> mailbox = new ArrayList<Mail>();
	List<String> hasMail = new ArrayList<String>();
	
	public Mailbox(){
		
	}
	
	public List<Mail> getMailFor(String name){
		// From, Message
		List<Mail> maillist = new ArrayList<Mail>();
		for(Mail mail : mailbox){
			if(mail.getRecipient().equals(name)){
				maillist.add(mail);
			}
		}
		return maillist;
	}
	
	public void addMail(Mail mail){
		this.mailbox.add(mail);
		this.hasMail.add(mail.getRecipient().toLowerCase());
	}
	
	public void removeMail(Mail mail){
		this.mailbox.remove(mail);
		this.hasMail.remove(mail.getRecipient().toLowerCase());
	}
	
	public void removeAllMail(ProxiedPlayer player){
		String name = player.getName().toLowerCase();
		List<Mail> maillist = getMailFor(name);
		for(int i = 0; i<maillist.size(); i++){
			Mail mail = maillist.get(i);
			if(mail.getRecipient().equalsIgnoreCase(name)){
				this.removeMail(mail);
			}
		}
	}
	
	public boolean hasMail(ProxiedPlayer player){
		String name = player.getName().toLowerCase();
        return this.hasMail.contains(name);
    }
	
	public boolean hasMail(String name){
        return this.hasMail.contains(name.toLowerCase());
    }

	public void readMail(ProxiedPlayer player) {
		ComponentBuilder msg = new ComponentBuilder("----------").color(ChatColor.GOLD);
		player.sendMessage(msg.create());
		List<Mail> maillist = getMailFor(player.getName().toLowerCase());
		
		if(maillist.size()==0){
			msg = new ComponentBuilder("You don't have any mail.").color(ChatColor.GREEN);
			player.sendMessage(msg.create());
		}else{
			for(Mail mail : maillist){
				msg = new ComponentBuilder("")
					.append("From: ").color(ChatColor.WHITE)
					.append(mail.getSender()).color(ChatColor.GOLD);
				player.sendMessage(msg.create());
	
				msg = new ComponentBuilder(mail.getMessage()).color(ChatColor.WHITE);
				player.sendMessage(msg.create());
				
				msg = new ComponentBuilder("----------").color(ChatColor.GOLD);
				player.sendMessage(msg.create());
			}
		}
	}
}
