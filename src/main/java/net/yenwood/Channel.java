package net.yenwood;

import net.dv8tion.jda.core.entities.Role;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.chat.ChatMessage;

import java.util.ArrayList;
import java.util.List;

public class Channel {
	public String name;
	public boolean publicity;
	public WoMChat plugin;
	public List<String> admins = new ArrayList<String>();
	public List<ProxiedPlayer> members = new ArrayList<ProxiedPlayer>();
	public List<ProxiedPlayer> listeners = new ArrayList<ProxiedPlayer>();
	public List<ProxiedPlayer> invited = new ArrayList<ProxiedPlayer>();
	public List<ProxiedPlayer> muted = new ArrayList<ProxiedPlayer>();
	public boolean permanentListeners;
	public boolean permanent;
	public boolean displayChannel;
	public ChatColor messageColor;
	public ChatColor nameColor;
	
	public Channel(String name, Boolean permlisteners, Boolean perm, Boolean publicity, WoMChat plugin){
		this.name = name;
		this.plugin = plugin;
		this.permanentListeners = permlisteners;
		this.permanent = perm;
		this.publicity = publicity;
		this.messageColor = ChatColor.WHITE;
		this.nameColor = ChatColor.WHITE;

        this.displayChannel = !name.equals("global");
		
		WoMChat.channels.put(this.name, this);
	}
	
	public Channel(String name, WoMChat plugin) {
		this(name, false, false, true, plugin);
	}

	public void setName(String name){
		this.name = name;
	}
	
	public void setPrivate(){
		this.publicity = false;
	}
	
	public void setPublic(){
		this.publicity = true;
	}
	
	public void addAdmin(String name){
		if(!this.admins.contains(name)){
			this.admins.add(name);
		}
	}
	
	public void removeAdmin(String name){
		if(this.admins.contains(name)){
			this.admins.remove(name);
		}
	}
	
	public boolean isAdmin(String name){
        return this.admins.contains(name);
    }
	
	public boolean isAdmin(ProxiedPlayer player){
        return this.admins.contains(player.getName());
    }

	public void addMember(ProxiedPlayer player) {
		if(!this.members.contains(player)){
			this.members.add(player);
			WoMChat.playerChannels.put(player, this);
		}
		
		if(!this.listeners.contains(player)){
			this.listeners.add(player);
		}
	}

	public void addListener(ProxiedPlayer player) {
		if(!this.listeners.contains(player)){
			this.listeners.add(player);
		}
	}

	public void removeMember(ProxiedPlayer player, boolean overrideperm) {
		if(this.members.contains(player)){
			this.members.remove(player);
		}
		if(this.members.size()==0&& !this.permanent){
			this.removeAllListeners();
			plugin.deleteChannel(this);
		}else
		if(this.listeners.contains(player)&&(!this.permanentListeners || overrideperm)){
			this.listeners.remove(player);
		}
	}

	public void removeListener(ProxiedPlayer player) {
		this.listeners.remove(player);
	}

	// Returns all members of this channel
	public List<ProxiedPlayer> getMembers(){
		return this.members;
	}
	
	// Returns all people that listen to this channel
	public List<ProxiedPlayer> getListeners(){
		return this.listeners;
	}
	
	public Channel removeAllMembers(){
		for(int i = 0; i<this.getListeners().size(); i++){
			ProxiedPlayer member = this.getListeners().get(i);
			this.removeMember(member, false);
			i--;
		}
		
		return this;
	}
	
	public Channel removeAllListeners(){
		for(int i = 0; i<this.getListeners().size(); i++){
			ProxiedPlayer listener = this.getListeners().get(i);
			this.removeListener(listener);
			i--;
		}
		
		return this;
	}

	// From SERVER
	public void sendMessage(ProxiedPlayer player, String serverName, String text) {
		ChatMessage message = new ChatMessage(serverName, player, text, this);
		TextComponent finalMessage = message.getServerMessage();

	    for(ProxiedPlayer plr : this.listeners){
			if((plugin.ignore.isIgnored(plr, player.getName()) && !player.hasPermission("womchat.unignorable")) || this.isMuted(player)){

			}else{
				plr.sendMessage(finalMessage);
			}
	    }

	    List<String> involved = new ArrayList<String>();
	    involved.add(name);
	    plugin.spies.sendMessage(finalMessage, this, involved);
	}
    public void sendMessage(String uuid, String username, String serverName, String text) {
        ChatMessage message = new ChatMessage(serverName, uuid, username, text, this);
        TextComponent finalMessage = message.getServerMessage();

        for(ProxiedPlayer plr : this.listeners){
            if(!plugin.ignore.isIgnored(plr, username) && !this.isMuted(uuid)){
                plr.sendMessage(finalMessage);
            }
        }

        List<String> involved = new ArrayList<String>();
        involved.add(name);
        plugin.spies.sendMessage(finalMessage, this, involved);
    }

	public void sendMessageTo(ProxiedPlayer player, String serverName, String text) {
		ChatMessage message = new ChatMessage(serverName, player, text, this);
		TextComponent finalMessage = message.getServerMessage();
		
	    player.sendMessage(finalMessage);
	}

	// From IRC/Discord
	public void sendMessage(String sender, List<Role> roles, String text) {
		List<String> strroles = new ArrayList<String>();
		ChatColor usernameColor = ChatColor.BLUE;
		
		// Username
		for(Role role : roles){
			strroles.add(role.getName().toLowerCase());
		}
		
		if(strroles.contains("staff")||strroles.contains("owner")){
			usernameColor = ChatColor.DARK_PURPLE;
		}else
		if(strroles.contains("official bots") || strroles.contains("bots") || strroles.contains("chur")){
			usernameColor = ChatColor.DARK_GREEN;
		}else{
			usernameColor = ChatColor.BLUE;
		}
		
		ChatMessage message = new ChatMessage("discord", sender, usernameColor, text, this);
		TextComponent finalMessage = message.getServerMessage();
	
		for(ProxiedPlayer plr : this.listeners){
			if(this.isMuted(plr)){
				
			}else{
				plr.sendMessage(finalMessage);
			}
		}
		    
		List<String> involved = new ArrayList<String>();
		plugin.spies.sendMessage(finalMessage, this, involved);
	}

	public String getName() {
		return this.name;
	}

	public boolean canJoin(ProxiedPlayer player) {
		if(this.publicity){
			return true;
		}else
		if(this.invited.contains(player)||player.hasPermission("womchat.staff")){
			return true;
		}
		return false;
	}

	public void invite(ProxiedPlayer player) {
		if(!this.invited.contains(player)){
			this.invited.add(player);
			ComponentBuilder msg = new ComponentBuilder("<WoMChat> You have been invited to join the channel "+this.getName()+". To join, type /channel join "+this.getName()).color(ChatColor.GREEN);
			player.sendMessage(msg.create());
		}
	}

	public boolean isListener(ProxiedPlayer player) {
        return this.listeners.contains(player);
    }

	public void setNameColor(ChatColor color) {
		this.nameColor = color;
	}

	public void setMessageColor(ChatColor color) {
		this.messageColor = color;
	}

	public void invite(ProxiedPlayer player, boolean silent) {
		if(!this.invited.contains(player)){
			this.invited.add(player);
		}
	}

	public boolean isInvited(ProxiedPlayer player) {
        return this.invited.contains(player);
    }

	public boolean isPublic() {
		return this.publicity;
	}

	public void addMute(ProxiedPlayer sender) {
		if(!this.muted.contains(sender)){
			this.muted.add(sender);
		}
	}

	public void removeMute(ProxiedPlayer sender) {
		if(this.muted.contains(sender)){
			this.muted.remove(sender);
		}
	}

    public boolean isMuted(ProxiedPlayer sender) {
        return this.muted.contains(sender);
    }
    public boolean isMuted(String sender) {
	    for(ProxiedPlayer player : this.muted){
	        if(player.getUniqueId().toString().replace("-", "").equalsIgnoreCase(sender)){
	            return true;
            }
        }
        return false;
    }
}
