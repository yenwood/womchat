package net.yenwood;

import lombok.Getter;
import net.dv8tion.jda.core.entities.Role;
import net.md_5.bungee.config.Configuration;

public class LocalConfiguration {
    private static Configuration config;
    @Getter public static String guildId;
    @Getter public static String host;
	@Getter public static String database;
	@Getter public static String username;
	@Getter public static String password;
	@Getter public static int port;
    @Getter public static String url;
    @Getter public static String forumUrl;
	@Getter public static String prefix;
	@Getter public static String tablename;
	@Getter public static String staffDescription;
	@Getter public static String trustedDescription;
	@Getter public static String memberDescription;

	@Getter public static Long warningCooldown;
	@Getter public static Long maxWarnings;
	@Getter public static Long messageCooldown;
	@Getter public static Long maxMessages;

    @Getter public static String region;
    @Getter public static String accessKey;
    @Getter public static String secretKey;
    @Getter public static boolean autoManageQueue;
    @Getter public static String queueName;
    @Getter public static Long queueSearchInterval;

	public LocalConfiguration(Configuration config){
	    LocalConfiguration.config = config;
		LocalConfiguration.host = config.getString("db_host");
		LocalConfiguration.database = config.getString("db_database");
		LocalConfiguration.username = config.getString("db_username");
		LocalConfiguration.password = config.getString("db_password");
		LocalConfiguration.port = config.getInt("db_port");
		LocalConfiguration.tablename = config.getString("db_table_name");

		LocalConfiguration.staffDescription = config.getString("descriptions.staff");
		LocalConfiguration.trustedDescription = config.getString("descriptions.trusted");
		LocalConfiguration.memberDescription = config.getString("descriptions.member");

		LocalConfiguration.warningCooldown = config.getLong("warning_cooldown") != 0 ? config.getLong("warning_cooldown") : 3600;
		LocalConfiguration.maxWarnings = config.getLong("max_warnings") != 0 ? config.getLong("max_warnings") : 3;
		LocalConfiguration.messageCooldown = config.getLong("message_cooldown") != 0 ? config.getLong("message_cooldown") : 2;
		LocalConfiguration.maxMessages = config.getLong("max_messages") != 0 ? config.getLong("max_messages") : 3;

        LocalConfiguration.url = "jdbc:mysql://" + LocalConfiguration.host + ":" + LocalConfiguration.port + "/" + LocalConfiguration.database;

        LocalConfiguration.forumUrl = config.getString("forum_url");

        LocalConfiguration.region = config.getString("sqs.region");
        LocalConfiguration.accessKey = config.getString("sqs.accessKey");
        LocalConfiguration.secretKey = config.getString("sqs.secretKey");
        LocalConfiguration.autoManageQueue = config.getBoolean("sqs.autoManageQueue");
        LocalConfiguration.queueName = config.getString("sqs.queueName");
        LocalConfiguration.queueSearchInterval = config.getLong("sqs.queueSearchInterval");
	}

	public static String getGroupNameFromRole(Role role){
	    return config.getString("rolegroups."+role.getName(), "null").equalsIgnoreCase("null") ? role.getName() : config.getString("rolegroups."+role.getName());
    }
}
