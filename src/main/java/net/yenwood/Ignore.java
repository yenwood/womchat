package net.yenwood;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Ignore {
	private HashMap<ProxiedPlayer, List<String>> list = new HashMap<ProxiedPlayer, List<String>>();
	private HashMap<String, List<String>> plainlist = new HashMap<String, List<String>>();
	private Configuration configuration;
	private WoMChat plugin;

	public Ignore(WoMChat plugin, Configuration config){
		this.configuration = config;
		this.plugin = plugin;
		
		for(String key : this.configuration.getKeys()){
			List<String> conflist = this.configuration.getStringList(key);
			this.plainlist.put(key, conflist);
		}
	}
	
	public void loadIgnores(ProxiedPlayer player){
		String name = player.getName();
		if(this.plainlist.containsKey(name)){
			this.list.put(player, plainlist.get(name));
		}
	}
	
	public boolean isIgnored(ProxiedPlayer player, String tocheck){
		tocheck = tocheck.toLowerCase();
		if(this.list.containsKey(player)){
			List<String> list = this.list.get(player);
			if(list.contains(tocheck)){
				return true;
			}
		}
		return false;
	}
	
	public void addIgnore(ProxiedPlayer player, String toignore){
		toignore = toignore.toLowerCase();
		if(this.list.containsKey(player)){
			List<String> list = this.list.get(player);
			if(!list.contains(toignore)){
				list.add(toignore);
			}
		}else{
			List<String> list = new ArrayList<String>();
			list.add(toignore);
			this.list.put(player, list);
		}
	}
	
	public void removeIgnore(ProxiedPlayer player, String toignore){
		toignore = toignore.toLowerCase();
		if(this.list.containsKey(player)){
			List<String> list = this.list.get(player);
			if(list.contains(toignore)){
				list.remove(toignore);
			}
		}
	}
	
	public void save(){
		for(Entry<ProxiedPlayer, List<String>> entry : this.list.entrySet()){
			String name = entry.getKey().getName();
			List<String> list = entry.getValue();
			this.configuration.set(name, list);
		}
		
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.configuration, new File(plugin.getDataFolder(), "ignores.yml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
