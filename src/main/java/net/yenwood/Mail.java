package net.yenwood;

public class Mail {
	private String recipient;
	private String sender;
	private String message = "";
	private Mailbox mailbox;
	
	public Mail(Mailbox mailbox){
		this.mailbox = mailbox;
	}
	
	public void setRecipient(String name){
		this.recipient = name;
	}
	
	public void setSender(String name){
		this.sender = name;
	}
	
	public void addText(String message){
		this.message = this.message + message;
	}
	
	public String getRecipient(){
		return this.recipient;
	}
	
	public String getSender(){
		return this.sender;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public void send(){
		this.mailbox.addMail(this);
	}
}
