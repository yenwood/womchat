package net.yenwood.jda;

import com.google.common.base.Joiner;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.Channel;
import net.yenwood.*;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Khorgain on 3/4/2017.
 */
public class MessageListener extends ListenerAdapter {
    private WoMChat plugin;

    public MessageListener(WoMChat plugin){
        this.plugin = plugin;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event){
        JDA jda = event.getJDA();
        long responseNumber = event.getResponseNumber();

        User author = event.getAuthor();
        Message message = event.getMessage();
        MessageChannel channel = event.getChannel();

        //String msg = message.getContent();
        String msg = message.getContentDisplay();
        boolean bot = author.isBot();

        // ignore bots
        if(bot){
            return;
        }

        if(event.isFromType(ChannelType.TEXT)){
            // Message is being sent from a guild channel
            TextChannel textChannel = event.getTextChannel();
            String channelId = textChannel.getId();
            Guild guild = event.getGuild();
            Member member = guild.getMember(author);
            List<Role> roles = member.getRoles();
            String name = member.getEffectiveName();

            boolean isStaff = channelId.equals(WoMConfig.getStaffID());
            boolean isCommand = channelId.equals(WoMConfig.getCommandID());
            boolean isGlobal = channelId.equals(WoMConfig.getGlobalID());
            boolean isOverwatch = channelId.equals(WoMConfig.getOverwatchID());

            // Only parse the message if it comes from a MC channel
            if(isStaff || isCommand || isGlobal || isOverwatch){

                if(msg.startsWith("s.")){
                    // starts with s. so lets trim off the s.
                    // and check what the command is
                    msg = msg.substring(2);

                    // split[0] = command
                    // split[1] = full arguments
                    String[] split = msg.split(" ", 2);
                    String command = split[0];
                    String arg = "";
                    if(split.length > 1){
                        arg = split[1];
                    }
                    String[] args = arg.split(" ");

                    switch(command){
                        case "c":
                            // s.c <text>
                            // <text> is sent to the staff channel if sent in staff, otherwise goes to global
                            if(isStaff){
                                plugin.staff.sendMessage(name, roles, arg);

                                // send discord message to overwatch
                                //JDABot.sendMessage(WoMConfig.getOverwatchID(), "<IRC>[global]"+name+" - "+arg);
                                String tentativeMessage = "<IRC>[global]"+name+" - "+arg;
                                MessageChannel targetChannel = JDABot.jda.getTextChannelById(WoMConfig.getOverwatchID());
                                String lastMsg = targetChannel.getLatestMessageId();
                                if(!targetChannel.getMessageById(lastMsg).complete().getContentRaw().equalsIgnoreCase(tentativeMessage)){
                                    // only send if the last message is NOT equal to the message we want to send
                                    JDABot.sendMessage(WoMConfig.getOverwatchID(), tentativeMessage);
                                }
                            }else if(isGlobal){
                                plugin.global.sendMessage(name, roles, arg);

                                // send discord message to overwatch
                                //JDABot.sendMessage(WoMConfig.getOverwatchID(), "<IRC>[global]"+name+" - "+arg);
                                String tentativeMessage = "<IRC>[global]"+name+" - "+arg;
                                MessageChannel targetChannel = JDABot.jda.getTextChannelById(WoMConfig.getOverwatchID());
                                String lastMsg = targetChannel.getLatestMessageId();
                                if(!targetChannel.getMessageById(lastMsg).complete().getContentRaw().equalsIgnoreCase(tentativeMessage)){
                                    // only send if the last message is NOT equal to the message we want to send
                                    JDABot.sendMessage(WoMConfig.getOverwatchID(), tentativeMessage);
                                }
                            }else if(isOverwatch){
                                plugin.global.sendMessage(name, roles, arg);

                                // send discord message to global
                                String tentativeMessage = "<IRC>[global]"+name+" - "+arg;
                                MessageChannel targetChannel = JDABot.jda.getTextChannelById(WoMConfig.getGlobalID());
                                String lastMsg = targetChannel.getLatestMessageId();
                                if(!targetChannel.getMessageById(lastMsg).complete().getContentRaw().equalsIgnoreCase(tentativeMessage)){
                                    // only send if the last message is NOT equal to the message we want to send
                                    JDABot.sendMessage(WoMConfig.getGlobalID(), tentativeMessage);
                                }
                            }
                            break;

                        case "list":
                            // s.list
                            // create a list of active users
                            String playerList = "";

                            Map<String, ServerInfo> servers = ProxyServer.getInstance().getServers();

                            for (Map.Entry<String, ServerInfo> entry : servers.entrySet()){
                                String serverName = entry.getKey();
                                ServerInfo serverInfo = entry.getValue();

                                String serverPrefix = WoMConfig.getServerPrefix(serverName);

                                playerList = playerList + serverPrefix+":";

                                Collection<ProxiedPlayer> players = serverInfo.getPlayers();
                                for(ProxiedPlayer player : players){
                                    String playername = player.getDisplayName();
                                    playerList = playerList + " " + playername;
                                }
                                playerList = playerList + "\n";
                            }

                            JDABot.sendMessage(channelId, playerList);
                            break;

                        case "channel":
                            // s.channel <channel name> <text>
                            // <text> is sent to <channel name>
                            if(WoMChat.channelExists(args[0])){
                                Channel chan = WoMChat.getChannel(args[0]);
                                args[0] = "";
                                String text = Joiner.on(" ").join(args);

                                // send message to mc channel
                                chan.sendMessage(name, roles, text);

                                if(isOverwatch && chan.isPublic()){
                                    // message was sent from overwatch and the channel is public so send to #global
                                    JDABot.sendMessage(WoMConfig.getGlobalID(), "<IRC>["+chan.getName()+"]"+name+" - "+text);
                                }else if(isGlobal && chan.isPublic()){
                                    // message was sent from global and the channel is public so send to #overwatch
                                    JDABot.sendMessage(WoMConfig.getOverwatchID(), "<IRC>["+chan.getName()+"]"+name+" - "+text);
                                }
                            }
                            break;

                        case "cmd":
                            // wip

                            break;

                        default:
                            // check if its a channel
                            if(WoMChat.channelExists(command)){
                                Channel chan = WoMChat.getChannel(command);
                                args[0] = "";
                                String text = Joiner.on(" ").join(args);

                                chan.sendMessage(name, roles, arg);
                                if(isOverwatch && chan.isPublic()){
                                    JDABot.sendMessage(WoMConfig.getGlobalID(), "<IRC>["+chan.getName()+"]"+name+" - "+text);
                                }else if(isGlobal && chan.isPublic()){
                                    JDABot.sendMessage(WoMConfig.getOverwatchID(), "<IRC>["+chan.getName()+"]"+name+" - "+text);
                                }
                            }
                    }
                }else{
                    // message doesn't start with s. so lets send to global or staff
                    if(isGlobal){
                        plugin.global.sendMessage(name, roles, msg);
                        JDABot.sendMessage(WoMConfig.getOverwatchID(), "<IRC>[global]"+name+" - "+msg);
                    }else if(isStaff){
                        plugin.staff.sendMessage(name, roles, msg);
                        JDABot.sendMessage(WoMConfig.getOverwatchID(), "<IRC>[staff]"+name+" - "+msg);
                    }

                }
            }
        }else if(event.isFromType(ChannelType.PRIVATE)){
            // message is a dm
            // check for verify

            if(msg.startsWith("!verify ")){
                String[] split = msg.split(" ", 2);
                String linkId = split[1];
                String url = LocalConfiguration.forumUrl;

                Guild guild = event.getJDA().getGuildById(WoMConfig.getGuildID());

                Role userRole = guild.getRolesByName("Verified Users", true).get(0);
                Role staffRole = guild.getRolesByName("Staff", true).get(0);
                Member member = guild.getMember(author);

                try {
                    JSONObject json = JsonReader.readJsonFromUrl(url+"/minecraft/verify_whitelist.php?linkid="+ URLEncoder.encode(linkId, "UTF-8")+"&discord="
                            +URLEncoder.encode(author.getId(), "UTF-8"));

                    if(json != null){
                        if(json.getBoolean("alreadylinked")){ // user is already linked
                            event.getPrivateChannel().sendMessage("You're already linked!").queue();
                        }else
                        if(json.has("uuid")){ // link id is right, uuid is returned
                            // put uuid and link id in a local db
                            event.getPrivateChannel().sendMessage("Your Discord and Sanctuary account have been linked!").queue();

                            WoMChat.db.update("INSERT INTO sanctuaryusers (uuid, link, discord) VALUES "+
                                    "('"+json.getString("uuid")+"', '"+linkId+"', '"+author.getId()+"')");

                            // set user's role to Users
                            if(!member.getRoles().contains(staffRole)){
                                guild.getController().addRolesToMember(guild.getMember(author), userRole).queue();
                            }
                        }else{  // link id is wrong
                            event.getPrivateChannel().sendMessage("That link ID isn't valid. Are you sure you entered it correctly?").queue();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
