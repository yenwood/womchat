package net.yenwood.jda;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.User;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.WoMChat;
import net.yenwood.WoMConfig;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Khorgain on 3/4/2017.
 */
public class JDABot {
    public static JDA jda;

    public JDABot(WoMChat plugin){
        try {
            jda = new JDABuilder(AccountType.BOT)
                    .setToken(WoMConfig.getToken())
                    .addEventListener(new MessageListener(plugin))
                    .buildBlocking();
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sendMessage(String channelID, String message){
        jda.getTextChannelById(channelID).sendMessage(message).queue();
    }

    public static void updateTopic(HashMap<ProxiedPlayer, String> players){
        List<String> playerList = new ArrayList<String>();

        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()){
            String playername = player.getDisplayName();
            playerList.add(playername);
        }

        String playerListJoined = String.join(", ", playerList);

        jda.getTextChannelById(WoMConfig.getGlobalID()).getManager().setTopic(playerListJoined).queue();
        jda.getTextChannelById(WoMConfig.getOverwatchID()).getManager().setTopic(playerListJoined).queue();
        jda.getTextChannelById(WoMConfig.getStaffID()).getManager().setTopic(playerListJoined).queue();
    }

    public static boolean userExists(String id){
        return jda.getUserById(id) != null;
    }

    public static User getUser(String id){
        return jda.retrieveUserById(id).complete();
    }

    public static String getUsername(String id){
        return getUser(id).getName();
    }

    public static void stop(){

    }
}
