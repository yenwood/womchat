package net.yenwood;

import net.md_5.bungee.config.Configuration;

import java.util.List;

public class WoMConfig {
	private static Configuration config;
	
	public WoMConfig(Configuration config){
		WoMConfig.config = config;
	}
	
	public static String getEmail(){
		String id = String.valueOf(config.get("discord.email"));
		
		return id!=null ? id : "!";
	}
	
	public static String getPassword(){
		String id = String.valueOf(config.get("discord.password"));
		
		return id!=null ? id : "!";
	}

	public static String getToken(){
		String id = String.valueOf(config.get("discord.token"));
		
		return id!=null ? id : "!";
	}
	
	public static String getGuildID(){
		String id = String.valueOf(config.get("discord.guild_id"));
		
		return id!=null ? id : "!";
	}
	
	public static String getOverwatchID(){
		String id = String.valueOf(config.get("discord.overwatch_channel_id"));
		
		return id!=null ? id : "!";
	}
	
	public static String getStaffID(){
		String id = String.valueOf(config.get("discord.staff_channel_id"));
		
		return id!=null ? id : "!";
	}
	
	public static String getCommandID(){
		String id = String.valueOf(config.get("discord.commands_channel_id"));
		
		return id!=null ? id : "!";
	}

	public static String getGlobalID(){
		String id = String.valueOf(config.get("discord.global_channel_id"));
		
		return id!=null ? id : "!";
	}

	public static String getServerPrefix(String server){
		String id = String.valueOf(config.get("prefixes.names."+server));
		String def = String.valueOf(config.get("prefixes.default.name"));
		
		return id.equals("null") ? def : id;
	}

	public static String getServerPrefixColor(String server){
		String id = String.valueOf(config.get("prefixes.colors."+server));
		String def = String.valueOf(config.get("prefixes.default.color"));

		return id.equals("null") ? def : id;
	}

	public static String getCommandRole(){
		String id = String.valueOf(config.get("discord.command_role_name"));
		
		return id!=null ? id : "!";
	}

	public static List<String> getCommands(){

        return config.getStringList("discord.runnable_commands");
	}

	public static String get(String path, String def){
		String id = String.valueOf(config.get(path));
		
		return id!=null ? id : def;
	}
	public static int getInt(String path, int def){
		int id = config.getInt(path);
		
		return id!=0 ? id : def;
	}
}
