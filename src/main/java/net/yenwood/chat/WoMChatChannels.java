package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.Channel;
import net.yenwood.WoMChat;

public class WoMChatChannels extends Command {
	WoMChat plugin;
	
	public WoMChatChannels(WoMChat plugin){
		super("channel");
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length<1){
			
		}else{
			switch(args[0].toLowerCase()){
			case "move":
				if(args.length != 3){
					sender.sendMessage("Wrong amount of arguments!");
					sender.sendMessage("/channel move <player> <channel>");
				}else{
					// args[0] = move
					// args[1] = player
					// args[2] = channel
					
					String channame = args[2].toLowerCase();
					String playername = args[1];
					
					ProxiedPlayer player = plugin.getPlayer(playername);
					Channel cchan = WoMChat.getChannel(player);
					Channel tchan = WoMChat.getChannel(channame);
					
					if(tchan == null){
						sender.sendMessage("The channel '"+channame+"' doesn't exist.");
						return;
					}
					
					cchan.removeMember(player, false);
					tchan.addMember(player);
				}
				break;
			case "create":
				if(args.length!=2){

				}else{
					// args[0] = create
					// args[1] = name of channel
					args[1] = args[1].toLowerCase();
					
					if(args[1].equals("invite")||args[1].equals("join")||args[1].equals("leave")||args[1].equals("list")||args[1].equals("color")){
						TextComponent msg = new TextComponent("<WoMChat> That is a reserved name. Please choose another.");
						msg.setColor(ChatColor.RED);
						sender.sendMessage(msg);
					}else
					if(!WoMChat.channelExists(args[1])){
						if(args[1].length()>6 && sender instanceof ProxiedPlayer){
							TextComponent msg = new TextComponent("<WoMChat> Channel name is too long. Try again with a shorter name.");
							msg.setColor(ChatColor.RED);
							sender.sendMessage(msg);
							return;
						}
						
						Channel nChannel = new Channel(args[1], plugin);
						WoMChat.addChannel(nChannel);
						
						if(sender instanceof ProxiedPlayer){
							nChannel.addAdmin(sender.getName());
							nChannel.invite((ProxiedPlayer)sender, true);

							TextComponent msg = new TextComponent("<WoMChat> You have created the channel " + nChannel.getName() + " successfully!");
							msg.setColor(ChatColor.GREEN);
							sender.sendMessage(msg);
							msg = new TextComponent("Use the command \"/channel join " + nChannel.getName() + "\" to join it!");
							msg.setColor(ChatColor.GREEN);
							sender.sendMessage(msg);
						}
					}else{
						TextComponent msg = new TextComponent("<WoMChat> That channel already exists. Please try again with a different name.");
						msg.setColor(ChatColor.RED);
						sender.sendMessage(msg);
					}
				}
				break;
				
			case "join":
				if(args.length!=2){

				}else{
					// args[0] = join
					// args[1] = name of channel
					args[1] = args[1].toLowerCase();

					if(!WoMChat.channelExists(args[1])){
                        if(args[1].equals("invite")||args[1].equals("join")||args[1].equals("leave")||args[1].equals("list")||args[1].equals("color")){
                            TextComponent msg = new TextComponent("<WoMChat> That is a reserved name. Please choose another.");
                            msg.setColor(ChatColor.RED);
                            sender.sendMessage(msg);
                        }else
                        if(!WoMChat.channelExists(args[1])){
                            if(args[1].length()>6 && sender instanceof ProxiedPlayer){
                                TextComponent msg = new TextComponent("<WoMChat> Channel name is too long. Try again with a shorter name.");
                                msg.setColor(ChatColor.RED);
                                sender.sendMessage(msg);
                                return;
                            }

                            Channel nChannel = new Channel(args[1], plugin);
                            WoMChat.addChannel(nChannel);

                            if(sender instanceof ProxiedPlayer){
                                nChannel.addAdmin(sender.getName());
                                nChannel.invite((ProxiedPlayer)sender, true);

                                TextComponent msg = new TextComponent("<WoMChat> You have created the channel " + nChannel.getName() + " successfully!");
                                msg.setColor(ChatColor.GREEN);
                                sender.sendMessage(msg);
                            }
                        }
                    }
					
					if(WoMChat.channelExists(args[1])){
						Channel jChannel = WoMChat.getChannel(args[1]);
						if(jChannel.canJoin((ProxiedPlayer)sender)){
							Channel pChannel = WoMChat.getChannel((ProxiedPlayer)sender);
							pChannel.removeMember((ProxiedPlayer)sender, false);
							jChannel.addMember((ProxiedPlayer)sender);
							
							if(jChannel.getName().equalsIgnoreCase("interpreter")){
								return;
							}
							
							if(WoMChat.getChannel((ProxiedPlayer)sender)==jChannel){
								TextComponent msg = new TextComponent("<WoMChat> You are now in the channel " + jChannel.getName());
								msg.setColor(ChatColor.GREEN);
								sender.sendMessage(msg);
							}else{
								TextComponent msg = new TextComponent("<WoMChat> An unexpected error occured while joining " + jChannel.getName());
								msg.setColor(ChatColor.RED);
								sender.sendMessage(msg);
							}
						}else{
							TextComponent msg = new TextComponent("<WoMChat> You are not allowed to join that channel.");
							msg.setColor(ChatColor.RED);
							sender.sendMessage(msg);
						}
					}
				}
				break;
				
			case "leave":
				if(args.length!=1){

				}else {
					// args[0] = leave
					Channel pChannel = WoMChat.getChannel(((ProxiedPlayer)sender));
					pChannel.removeMember(((ProxiedPlayer)sender), false);
					plugin.global.addMember(((ProxiedPlayer)sender));
					
					TextComponent msg = new TextComponent("<WoMChat> You are now in the channel global");
					msg.setColor(ChatColor.GREEN);
					sender.sendMessage(msg);
				}
				break;

			case "list":
				if(args.length!=2){
					// args[0] = list
					TextComponent msg = new TextComponent("<WoMChat> Channels that you can join:");
					msg.setColor(ChatColor.GREEN);
					sender.sendMessage(msg);
					for(Channel channel : WoMChat.channels.values()){
						if(channel.canJoin(((ProxiedPlayer)sender))){
							int members = channel.getMembers().size();
							TextComponent channelName = new TextComponent(channel.getName());
							TextComponent memberCount = new TextComponent(": "+members+" members");
							
							if(channel.isPublic()){
								channelName.setColor(ChatColor.GOLD);
							}else
							if(channel.isInvited(((ProxiedPlayer)sender))){
								channelName.setColor(ChatColor.GREEN);
							}else{
								channelName.setColor(ChatColor.RED);
							}
							memberCount.setColor(ChatColor.WHITE);

							TextComponent msg2 = channelName;
							msg2.addExtra(memberCount);
							
							sender.sendMessage(msg2);
						}
					}
				}else{
					// args[0] = list
					// args[1] = name of channel
					args[1] = args[1].toLowerCase();
					
					if(WoMChat.channelExists(args[1])){
						Channel channel = WoMChat.getChannel(args[1]);

						TextComponent msg = new TextComponent("<WoMChat> Members of channel " + channel.getName() + ":");
						msg.setColor(ChatColor.GREEN);
						sender.sendMessage(msg);
						for(ProxiedPlayer playerr : channel.getMembers()){
							TextComponent msg2 = new TextComponent(playerr.getName());
							msg2.setColor(ChatColor.GOLD);
							sender.sendMessage(msg2);
						}
					}else{
						sendError(((ProxiedPlayer)sender), "ChannelDoesntExist");
					}
				}
				break;
				
			case "invite":
				if(args.length!=3 && args.length!=2){
					TextComponent msg = new TextComponent("<WoMChat> Proper usage: /channel invite <channelname> <playername>");
					msg.setColor(ChatColor.RED);
					sender.sendMessage(msg);
				}else{
					if(args.length == 2){
						// args[0] = invite
						// args[1] = player
						ProxiedPlayer invited = ProxyServer.getInstance().getPlayer(args[1]);
						Channel pchannel = plugin.global;
						
						if(sender instanceof ProxiedPlayer){
							// sender is a player
							ProxiedPlayer player = (ProxiedPlayer)sender;
							pchannel = WoMChat.getChannel(player);
						}
						
						pchannel.invite(invited);
						
						TextComponent msg = new TextComponent("<WoMChat> User has been invited!");
						msg.setColor(ChatColor.GREEN);
						sender.sendMessage(msg);
					}else {
                        // args[0] = invite
                        // args[1] = name of channel
                        // args[2] = player
                        args[1] = args[1].toLowerCase();

                        if(WoMChat.channelExists(args[1])){
                            Channel channel = WoMChat.getChannel(args[1]);
                            ProxiedPlayer invited = ProxyServer.getInstance().getPlayer(args[2]);
                            if(sender instanceof ProxiedPlayer){
                                // Sender is player
                                if(channel.isAdmin(((ProxiedPlayer)sender))){
                                    channel.invite(invited, false);
                                    TextComponent msg = new TextComponent("<WoMChat> User has been invited!");
                                    msg.setColor(ChatColor.GREEN);
                                    sender.sendMessage(msg);
                                }
                            }else{
                                // sender is console

                                channel.invite(invited, false);
                                TextComponent msg = new TextComponent("<WoMChat> User has been invited!");
                                msg.setColor(ChatColor.GREEN);
                                sender.sendMessage(msg);
                            }
                        }else{
                            sendError(((ProxiedPlayer)sender), "ChannelDoesntExist");
                        }

                    }
				}
				break;
				
			case "mute":
				if(args.length!=2){
					TextComponent msg = new TextComponent("<WoMChat> Proper usage: /channel mute <channelname>");
					msg.setColor(ChatColor.RED);
					sender.sendMessage(msg);
				}else{
					// args[0] = mute
					// args[1] = name of channel
					args[1] = args[1].toLowerCase();
					
					if(WoMChat.channelExists(args[1])){
						Channel channel = WoMChat.getChannel(args[1]);
						if(channel.isMuted(((ProxiedPlayer)sender))){
							// Unmute
							channel.removeMute(((ProxiedPlayer)sender));
							TextComponent msg = new TextComponent("<WoMChat> "+channel.getName()+" is now unmuted.");
							msg.setColor(ChatColor.GREEN);
							sender.sendMessage(msg);
						}else{
							// Mute
							channel.addMute(((ProxiedPlayer)sender));
							TextComponent msg = new TextComponent("<WoMChat> "+channel.getName()+" is now muted.");
							msg.setColor(ChatColor.GREEN);
							sender.sendMessage(msg);
						}
					}else{
						sendError(((ProxiedPlayer)sender), "ChannelDoesntExist");
					}
				}
				break;
				
			case "unmute":
				if(args.length!=2){
					TextComponent msg = new TextComponent("<WoMChat> Proper usage: /channel mute <channelname>");
					msg.setColor(ChatColor.RED);
					sender.sendMessage(msg);
				}else{
					// args[0] = mute
					// args[1] = name of channel
					args[1] = args[1].toLowerCase();
					
					if(WoMChat.channelExists(args[1])){
						Channel channel = WoMChat.getChannel(args[1]);
						if(channel.isMuted(((ProxiedPlayer)sender))){
							// Unmute
							channel.removeMute(((ProxiedPlayer)sender));
							TextComponent msg = new TextComponent("<WoMChat> "+channel.getName()+" is now unmuted.");
							msg.setColor(ChatColor.GREEN);
							sender.sendMessage(msg);
						}else{
							TextComponent msg = new TextComponent("<WoMChat> "+channel.getName()+" isn't muted.");
							msg.setColor(ChatColor.RED);
							sender.sendMessage(msg);
						}
					}else{
						sendError(((ProxiedPlayer)sender), "ChannelDoesntExist");
					}
				}
				break;
				
			case "color":
				if(args.length!=3){
					TextComponent msg = new TextComponent("<WoMChat> Proper usage: /channel color <name> <color>");
					msg.setColor(ChatColor.RED);
					sender.sendMessage(msg);
				}else{
					// args[0] = color
					// args[1] = name
					// args[2] = color
					TextComponent msg = new TextComponent("");
					args[2] = args[2].toUpperCase();
					Channel channel = WoMChat.getChannel(args[1]);
					
					if(channel == null){
						msg = new TextComponent("<WoMChat> "+args[1]+" doesn't exist. Please try again.");
						msg.setColor(ChatColor.RED);
						sender.sendMessage(msg);
						return;
					}
					
					if(args[2].equals("LIGHT_PURPLE")){
						msg = new TextComponent("<WoMChat> "+args[2]+" is a unique reserved color. Please try again.");
						msg.setColor(ChatColor.RED);
						sender.sendMessage(msg);
					}else{
						try{
							channel.setNameColor(ChatColor.valueOf(args[2]));
							
							msg = new TextComponent("<WoMChat> "+args[1]+"'s name color is now "+args[2]+".");
							msg.setColor(ChatColor.RED);
							sender.sendMessage(msg);
						}catch(IllegalArgumentException e){
							msg = new TextComponent("<WoMChat> "+args[2]+" isn't a proper color. Try again.");
							msg.setColor(ChatColor.RED);
							sender.sendMessage(msg);
						}
					}
				}
				break;
			}
			
			// args[0] didn't fit any commands, assume channel name
			if(WoMChat.channelExists(args[0])){
				ProxiedPlayer player = (ProxiedPlayer) sender;
				// args[0] channel name
				// args[1] OPTIONAL setting | invite | public | private
				// args[2] OPTIONAL new setting value
				args[0] = args[0].toLowerCase();
				
				Channel channel = WoMChat.getChannel(args[0]);
				TextComponent msg = new TextComponent("");
				
				if(args.length==1){
					// Give channel info.
					
				}else
				if(args.length==2){
					// Private/public
					if(channel.isAdmin(player)){
						switch(args[1].toLowerCase()){
						case "private":
							channel.setPrivate();
							msg = new TextComponent("<WoMChat> The channel "+channel.getName()+" is now private!");
							msg.setColor(ChatColor.GREEN);
							player.sendMessage(msg);
							break;
						case "public":
							channel.setPublic();
							msg = new TextComponent("<WoMChat> The channel "+channel.getName()+" is now public!");
							msg.setColor(ChatColor.GREEN);
							player.sendMessage(msg);
							break;
						}
					}else{
						msg = new TextComponent("<WoMChat> You have to be an admin of this channel to do that.");
						msg.setColor(ChatColor.RED);
						player.sendMessage(msg);
					}
				}else
				if(args.length==3){
					// invite
					// promote
					// demote
					if(channel.isAdmin(player)){
						switch(args[1].toLowerCase()){
						case "invite":
							if(plugin.isOnline(args[2])){
								ProxiedPlayer toInvite = plugin.getPlayer(args[2]);
								channel.invite(toInvite, false);
								msg = new TextComponent("<WoMChat> " + toInvite.getName() + " has been invited to " + channel.getName() + "!");
								msg.setColor(ChatColor.GREEN);
								player.sendMessage(msg);
							}else{
								msg = new TextComponent("<WoMChat> That player isn't online.");
								msg.setColor(ChatColor.RED);
								player.sendMessage(msg);
							}
							break;
						case "promote":
							String toPromote = args[2];
							channel.addAdmin(toPromote);
							msg = new TextComponent("<WoMChat> " + toPromote + " has been added as an admin of " + channel.getName() + "!");
							msg.setColor(ChatColor.GREEN);
							player.sendMessage(msg);
							break;
						case "demote":
							String toDemote = args[2];
							if(channel.isAdmin(toDemote)){
								channel.removeAdmin(toDemote);
								msg = new TextComponent("<WoMChat> " + toDemote + " has been removed as an admin of " + channel.getName() + "!");
								msg.setColor(ChatColor.GREEN);
								player.sendMessage(msg);
							}else{
								msg = new TextComponent("<WoMChat> " + toDemote + " isn't an admin of " + channel.getName() + "!");
								msg.setColor(ChatColor.RED);
								player.sendMessage(msg);
							}
							break;
						}
					}else{
						msg = new TextComponent("<WoMChat> You have to be an admin of this channel to do that.");
						msg.setColor(ChatColor.RED);
						player.sendMessage(msg);
					}
				}else{
					msg = new TextComponent("<WoMChat> Invalid amount of arguments.");
					msg.setColor(ChatColor.RED);
					player.sendMessage(msg);
					msg = new TextComponent("Usage: /channel <name>");
					msg.setColor(ChatColor.RED);
					player.sendMessage(msg);
					msg = new TextComponent("Usage: /channel <name> [public|private]");
					msg.setColor(ChatColor.RED);
					player.sendMessage(msg);
					msg = new TextComponent("Usage: /channel <name> invite <player>");
					msg.setColor(ChatColor.RED);
					player.sendMessage(msg);
					msg = new TextComponent("Usage: /channel <name> <setting> <value>");
					msg.setColor(ChatColor.RED);
					player.sendMessage(msg);
				}
			}
		}
	}
	
	private void sendError(ProxiedPlayer player, String reason){
		TextComponent msg = new TextComponent("<WoMChat> An unexpected error occured. Please contact an administrator.");
		
		switch(reason){
		case "NotEnoughArguments":
			
			break;
		case "ChannelDoesntExist":
			msg = new TextComponent("<WoMChat> That channel doesn't exist.");

			msg.setColor(ChatColor.RED);
			player.sendMessage(msg);
			break;
		}
	}

}
