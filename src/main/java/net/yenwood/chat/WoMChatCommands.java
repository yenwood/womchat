package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.Descriptions;
import net.yenwood.Designations;
import net.yenwood.WoMChat;
import net.yenwood.jda.JDABot;

public class WoMChatCommands extends Command {
	WoMChat plugin;
	
	public WoMChatCommands(WoMChat plugin){
		super("womchat");
		this.plugin = plugin;
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		// Check args length
		if(args.length<1){
			ComponentBuilder msg1 = new ComponentBuilder("<WoMChat> Sorry, that command needs more arguments.").color(ChatColor.RED);
			sender.sendMessage(msg1.create());
		}else
		// startbot command
		if(args[0].equalsIgnoreCase("startbot")){
			if(sender.hasPermission("womchat.startbot")||!(sender instanceof ProxiedPlayer)){
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> Starting bot.").color(ChatColor.GREEN);
				sender.sendMessage(msg.create());
				plugin.startBot();
			}else{
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> You do not have the proper permissions for this command.").color(ChatColor.RED);
				sender.sendMessage(msg.create());
			}
		}else
		// stopbot command
		if(args[0].equalsIgnoreCase("stopbot")){
			if(sender.hasPermission("womchat.stopbot")||!(sender instanceof ProxiedPlayer)){
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> Stopping bot.").color(ChatColor.GREEN);
				sender.sendMessage(msg.create());
				JDABot.stop();
			}else{
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> You do not have the proper permissions for this command.").color(ChatColor.RED);
				sender.sendMessage(msg.create());
			}
		}else
		// Reload config command
		if(args[0].equalsIgnoreCase("reload")){
			if(sender.hasPermission("womchat.reload")){
				plugin.loadConfig();
			}
		}else
		// Change descriptions
		if(args[0].equalsIgnoreCase("description")){
			if(args.length<3){
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> Proper usage: /description <player> <description>").color(ChatColor.RED);
				sender.sendMessage(msg.create());
				return;
			}
			// args[0] = "description"
			// args[1] = player
			// args[2] = description
			if(sender.hasPermission("womchat.description")||!(sender instanceof ProxiedPlayer)){
				ProxiedPlayer player = plugin.getPlayer(args[1]);
				if(player!=null){
					Descriptions.set(player, args[2]);
					
					ComponentBuilder msg = new ComponentBuilder("<WoMChat> Description for "+player.getName()+" changed successfully.").color(ChatColor.RED);
					sender.sendMessage(msg.create());
				}else{
					ComponentBuilder msg = new ComponentBuilder("<WoMChat> That player is not online.").color(ChatColor.RED);
					sender.sendMessage(msg.create());
				}
			}else{
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> You do not have the proper permissions for this command.").color(ChatColor.RED);
				sender.sendMessage(msg.create());
			}
		}else
		// Change designations
		if(args[0].equalsIgnoreCase("designation")){
			if(args.length<3||args.length>3){
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> Proper usage: /designation <player> <designation>").color(ChatColor.RED);
				sender.sendMessage(msg.create());
				return;
			}
			// args[0] = "designation"
			// args[1] = player
			// args[2] = designation in the format "D1"
			if(sender.hasPermission("womchat.designation")||!(sender instanceof ProxiedPlayer)){
				Designations.set(args[1], args[2]);
					
				if(sender instanceof ProxiedPlayer){
					ComponentBuilder msg = new ComponentBuilder("<WoMChat> Designation for "+args[1]+" changed successfully.").color(ChatColor.RED);
					sender.sendMessage(msg.create());
				}
			}else{
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> You do not have the proper permissions for this command.").color(ChatColor.RED);
				sender.sendMessage(msg.create());
			}
		}
	}

}
