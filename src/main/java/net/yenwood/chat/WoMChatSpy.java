package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.WoMChat;

public class WoMChatSpy extends Command {

	private WoMChat plugin;

	public WoMChatSpy(WoMChat plugin) {
		super("chatspy");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(sender.hasPermission("womchat.spy")){
			ProxiedPlayer player = (ProxiedPlayer) sender;
			if(!plugin.spies.contains(player)){
				plugin.spies.add(player);
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> You are now listening to all channels!");
				msg.color(ChatColor.GREEN);
				sender.sendMessage(msg.create());
			}else{
				plugin.spies.remove(player);
				ComponentBuilder msg = new ComponentBuilder("<WoMChat> You have stopped listening to all channels!");
				msg.color(ChatColor.GREEN);
				sender.sendMessage(msg.create());
			}
		}else{
			ComponentBuilder msg = new ComponentBuilder("<WoMChat> You don't have permission to use this.");
			msg.color(ChatColor.RED);
			sender.sendMessage(msg.create());
		}
	}

}
