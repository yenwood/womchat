package net.yenwood.chat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.User;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.exceptions.PermissionException;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.yenwood.*;
import net.yenwood.chatrestriction.ChatRestrictHandler;
import net.yenwood.jda.JDABot;
import net.yenwood.sqsfifo.SQSMessage;
import net.yenwood.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import static net.yenwood.WoMChat.sqsfifo;

public class WoMChatListener implements Listener {
	WoMChat plugin;
	JDABot bot;
	Gson gson;
	
	HashMap<ProxiedPlayer, String> plist = new HashMap<ProxiedPlayer, String>();
	
	public WoMChatListener (WoMChat plugin, JDABot bot){
		this.plugin = plugin;
		this.bot = bot;
        this.gson = new GsonBuilder().create();
	}
	
	// Player joins proxy
	@EventHandler
	public void onPostLogin(PostLoginEvent event){
		final ProxiedPlayer player = (ProxiedPlayer) event.getPlayer();
		String name = player.getDisplayName();
        UUID rawuuid = player.getUniqueId();
        String uuid = player.getUniqueId().toString().replace("-", "");
		
		// TODO command
		WoMChat.designations.put(uuid, Designations.get(uuid));
		
		// Add player to the global channel
		plugin.global.addMember(player);
        plugin.online.put(player.getName().toLowerCase(), player);
        //plugin.onlineAllProxies.put(player.getName().toLowerCase(), new OnlinePlayerData(uuid, name, ProxyServer.getInstance().getName()));

		if(player.hasPermission("womchat.staff")||player.hasPermission("womchat.trusted")){
			plugin.staff.invite(player, false);
			plugin.staff.addListener(player);
		}
		
		if(player.hasPermission("womchat.interpreter")){
			plugin.interpreter.invite(player, true);
		}
		
		if(plugin.spies.contains(player)){
			plugin.spies.add(player);
		}
		
		if(plugin.mailbox.hasMail(player)){
    		ComponentBuilder msg = new ComponentBuilder("You have mail!").color(ChatColor.GREEN);
    		player.sendMessage(msg.create());
    		msg = new ComponentBuilder("Type ").color(ChatColor.GOLD)
    				.append("/mail read").color(ChatColor.RED)
    				.append(" to read it all!").color(ChatColor.GOLD);
    		player.sendMessage(msg.create());
		}

        // get user from db using their uuid
        try {
            ResultSet rs = WoMChat.db.query("SELECT * FROM sanctuaryusers WHERE `uuid`='"+uuid+"'");
            if(rs.next()){
                String discordid = rs.getString("discord");

                Guild guild = JDABot.jda.getGuildById(WoMConfig.getGuildID());
                Member member = guild.getMemberById(discordid);

                for(Role role : member.getRoles()){
                    String groupName = LocalConfiguration.getGroupNameFromRole(role);
                    if(groupName != null){
                        LuckPermsApi api = LuckPerms.getApi();

                        //CompletableFuture<Optional<Group>> future = api.getGroupManager().loadGroup(groupName);

                        Node node = api.getNodeFactory().makeGroupNode(groupName).build();
                        User user = api.getUserManager().getUser(player.getUniqueId());
                        user.setPermission(node);
                        api.getUserManager().saveUser(user);
                    }
                }

                try{
                    guild.getController().setNickname(member, name).queue();
                } catch (PermissionException e){

                }

                rs.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	
	@EventHandler
	public void onServerConnected(ServerConnectedEvent event){
		final ProxiedPlayer player = (ProxiedPlayer) event.getPlayer();
		String server = event.getServer().getInfo().getName();

		// Send message to IRC
		if(this.plist.containsKey(player)){
			JDABot.sendMessage(WoMConfig.getOverwatchID(),
				"*_" + player.getName() + " joined the server " + event.getServer().getInfo().getName() + "_*");
			
			this.plist.put(player, server);
		}else{
            JDABot.sendMessage(WoMConfig.getOverwatchID(),
				"*_" + player.getName() + " logged on and joined the server "+server+"_*");

            JDABot.sendMessage(WoMConfig.getGlobalID(),
				"*_" + player.getName() + " logged on and joined the server "+server+"_*");
			
			this.plist.put(player, server);
		}

		JDABot.updateTopic(this.plist);
	}
	
	@EventHandler
	public void onPlayerDisconnect(PlayerDisconnectEvent event){
		final ProxiedPlayer player = (ProxiedPlayer) event.getPlayer();
		String uuid = player.getUniqueId().toString();
		
		// Remove player from their channel
		WoMChat.getChannel(player).removeMember(player, true);
		plugin.global.removeMember(player, true);
		plugin.global.removeListener(player);
		plugin.staff.removeListener(player);
		plugin.online.remove(player.getName().toLowerCase());
		
		// Clear designation
		WoMChat.designations.remove(uuid);

		// Send message to IRC
        JDABot.sendMessage(WoMConfig.getOverwatchID(), "*_" + player.getName() + " has left the server_*");
        JDABot.sendMessage(WoMConfig.getGlobalID(), "*_" + player.getName() + " has left the server_*");
		
		this.plist.remove(player);

        JDABot.updateTopic(this.plist);
	}
	
	@EventHandler
	public void onTabComplete(TabCompleteEvent ev){
		 for (Entry<String, ProxiedPlayer> p : plugin.online.entrySet()){
			 String name = p.getKey();
			 
			 if (name.toLowerCase().startsWith(ev.getCursor().toLowerCase())){
				 ev.getSuggestions().add(name);
			 }
		 }
	}
    
    @EventHandler
    public void onChatEvent(ChatEvent event) {
    	// When a player says something...
        final ProxiedPlayer sender = (ProxiedPlayer) event.getSender();
    	final String serverName = sender.getServer().getInfo().getName();
        String message = event.getMessage();
        
        if(WoMChat.bspinputs.contains(sender.getUniqueId().toString())){
            WoMChat.bspinputs.remove(sender.getUniqueId().toString());
        	return;
        }
        
        // Check mailbox
		if(plugin.mailbox.hasMail(sender)){
    		ComponentBuilder msg = new ComponentBuilder("You have mail!").color(ChatColor.GREEN);
    		sender.sendMessage(msg.create());
    		msg = new ComponentBuilder("Type ").color(ChatColor.GOLD)
    				.append("/mail read").color(ChatColor.RED)
    				.append(" to read it all!").color(ChatColor.GOLD);
    		sender.sendMessage(msg.create());
		}
		
		if(!event.isCommand()){
			SpamHandler.process(sender);
		}
		if(!event.isCommand()&&ChatRestrictHandler.isRestricted(sender)){
			if(ChatRestrictHandler.getMessages(sender) <= 0){
				TextComponent message1 = new TextComponent("");
				message1.addExtra("You've run out of messages!");
				message1.setColor(ChatColor.RED);
				sender.sendMessage(message1);
				event.setCancelled(true);
				return;
			}else{
				ChatRestrictHandler.getChatRestrict(sender).decrementMessages();
			}
		}
        
        // Make sure it isn't a command.
        if(!event.isCommand()){
    		
        	if(event.isCancelled()){
	    		Channel channel = WoMChat.getChannel(sender);

                ChatMessage msg = new ChatMessage(serverName, sender, message, channel);
	    		TextComponent finalMessage = msg.getServerMessage();
        		
        		sender.sendMessage(finalMessage);
        	}else{
	        	event.setCancelled(true);
	    		Channel channel = WoMChat.getChannel(sender);
	    		
	    		if(channel == null){
	    			ComponentBuilder msg = new ComponentBuilder("You aren't in a channel. Try /channel join global").color(ChatColor.RED);
	        		sender.sendMessage(msg.create());
	    		}else{
	    			// !Global chat prefix
		        	if(message.substring(0,1).equals("!")&&!channel.getName().equals("global")){
		        		message = message.replaceFirst("!", "");
		        		plugin.global.sendMessage(sender, serverName, message);
		        		channel = plugin.global;

                        Gson gson = new GsonBuilder().create();

                        SQSMessage sqsMessage = new SQSMessage();
                        sqsMessage.uuid = sender.getUniqueId().toString();
                        sqsMessage.username = sender.getName();
                        sqsMessage.type = "chatmessage";
                        sqsMessage.channel = channel.getName();
                        sqsMessage.content = message;
                        sqsMessage.source = serverName;
                        sqsMessage.timestamp = ""+Util.getNow();

                        sqsfifo.sendMessage(gson.toJson(sqsMessage));
		        	}else // @Staff chat prefix
			        if(message.substring(0,1).equals("@")&&plugin.staff.canJoin(sender)){
			        	message = message.replaceFirst("@", "");
			        	plugin.staff.sendMessage(sender, serverName, message);
		        		channel = plugin.staff;

                        Gson gson = new GsonBuilder().create();

                        SQSMessage sqsMessage = new SQSMessage();
                        sqsMessage.uuid = sender.getUniqueId().toString();
                        sqsMessage.username = sender.getName();
                        sqsMessage.type = "chatmessage";
                        sqsMessage.channel = channel.getName();
                        sqsMessage.content = message;
                        sqsMessage.source = serverName;
                        sqsMessage.timestamp = ""+Util.getNow();

                        sqsfifo.sendMessage(gson.toJson(sqsMessage));
			        }else // #channel Channel chat prefix
			        if(message.substring(0,1).equals("#")){
			        	String chanName = message.substring(0, message.indexOf(' ')).replaceFirst("#", "");
			        	message = message.substring(message.indexOf(' '));
			        	if(WoMChat.channelExists(chanName)){
						    channel = WoMChat.getChannel(chanName);
			        		if(channel.canJoin(sender)){
							    channel.sendMessage(sender, serverName, message);
                                Gson gson = new GsonBuilder().create();

                                SQSMessage sqsMessage = new SQSMessage();
                                sqsMessage.uuid = sender.getUniqueId().toString();
                                sqsMessage.username = sender.getName();
                                sqsMessage.type = "chatmessage";
                                sqsMessage.channel = channel.getName();
                                sqsMessage.content = message;
                                sqsMessage.source = serverName;
                                sqsMessage.timestamp = ""+Util.getNow();

                                sqsfifo.sendMessage(gson.toJson(sqsMessage));
			        		}else{
				        		ComponentBuilder msg = new ComponentBuilder("You aren't allowed to speak into that channel.").color(ChatColor.RED);
				        		sender.sendMessage(msg.create());
			        		}
			        	}else{
			        		ComponentBuilder msg = new ComponentBuilder("That channel doesn't exist.").color(ChatColor.RED);
			        		sender.sendMessage(msg.create());
			        	}
			        }else // If interpreter mode is active
			        if(channel.getName().equals("interpreter")){
			        	event.setMessage(message);
			        	event.setCancelled(false);
			        	return;
			        }else{
			        	channel.sendMessage(sender, serverName, message);

                        Gson gson = new GsonBuilder().create();

                        SQSMessage sqsMessage = new SQSMessage();
                        sqsMessage.uuid = sender.getUniqueId().toString();
                        sqsMessage.username = sender.getName();
                        sqsMessage.type = "chatmessage";
                        sqsMessage.channel = channel.getName();
                        sqsMessage.content = message;
                        sqsMessage.source = serverName;
                        sqsMessage.timestamp = ""+Util.getNow();

                        sqsfifo.sendMessage(gson.toJson(sqsMessage));
			        }

		        	message = message.replace("*", "\\*");
		        	message = message.replace("_", "\\_");

		        	String username = sender.getName();
		        	username = username.replace("*", "\\*");
		        	username = username.replace("_", "\\_");
		        	
		        	String channame = channel.getName();
		        	channame = channame.replace("*", "\\*");
		        	channame = channame.replace("_", "\\_");

		        	String server = WoMConfig.getServerPrefix(sender.getServer().getInfo().getName());
		        	
		        	// Send message to IRC
		        	if(channel.isPublic()){
                        JDABot.sendMessage(WoMConfig.getGlobalID(),
		        			"<" + server + ">["+channame+"]" + username + " - " + message);
		        	}else if(channel.getName().equals("staff")){
                        JDABot.sendMessage(WoMConfig.getStaffID(),
		        			"<" + server + ">["+channame+"]" + username + " - " + message);
		        	}
                    JDABot.sendMessage(WoMConfig.getOverwatchID(),
		        		"<" + server + ">["+channame+"]" + username + " - " + message);
	    		}
        	}
        }else{
        	// Command executed
            String server = WoMConfig.getServerPrefix(sender.getServer().getInfo().getName());
            JDABot.sendMessage(WoMConfig.getCommandID(), "<" + server + ">"+sender.getName() + " executed: " + message);
        }
    }
}