package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.Channel;
import net.yenwood.WoMChat;
import net.yenwood.WoMConfig;
import net.yenwood.jda.JDABot;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatMessage {
	private String serverName;
	private String message;
	private TextComponent messageText;
	private Channel channel;
	
	// Only used if message is from server
	private ProxiedPlayer player;
	
	// Only used if message is from Discord
    private String playerName;
    private String uuid;
	private ChatColor playerNameColor;

	public ChatMessage(ProxiedPlayer player){
		this.player = player;
	}

    public ChatMessage(String serverName, ProxiedPlayer player, String message, Channel channel){
        this.serverName = serverName;
        this.player = player;
        this.playerName = player.getDisplayName();
        this.message = message;
        this.channel = channel;

        this.messageText = this.filterText(message);
    }
    public ChatMessage(String serverName, String uuid, String username, String message, Channel channel){
        this.serverName = serverName;
        this.uuid = uuid;
        this.playerName = username;
        this.message = message;
        this.channel = channel;

        this.messageText = this.filterText(message);
    }
	
	public ChatMessage(String serverName, String sender, ChatColor playerNameColor, String message, Channel channel) {
		this.serverName = serverName;
		this.playerName = sender;
		this.playerNameColor = playerNameColor;
		this.message = message;
		this.channel = channel;
		
		this.messageText = this.filterText(message);
	}
	
	private TextComponent filterText(String text){
		String[] words = text.trim().split(" ");
		TextComponent msg = new TextComponent("");
		
		// Message text
		for(String word : words){
			// complementary space
        	msg.addExtra(" ");
        	
            // URL Check
            Pattern url = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Pattern mentionPattern = Pattern.compile("<@(.*?)>");
            
            if(url.matcher(word).find()) {
            	TextComponent link = new TextComponent(word);
            	link.setClickEvent(new ClickEvent(Action.OPEN_URL, word));
            	msg.addExtra(link);
            }else
            if(WoMChat.filter.test(word) && (player != null && !player.hasPermission("womchat.filter.bypass"))){
            	msg.addExtra(WoMChat.filter.replace(word));
            }else
            if(mentionPattern.matcher(word).find()){
                Matcher matcher = mentionPattern.matcher(word);
                matcher.matches();
                String userid = matcher.group(1);
                if(JDABot.userExists(userid)){
                	msg.addExtra(JDABot.getUser(userid).getName());
                }
            }else{
            	msg.addExtra(word);
            }
        }
		
		return msg;
	}

	public String getServerName(){
		return this.serverName;
	}
	
	public ProxiedPlayer getPlayer(){
		return this.player;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public Channel getChannel(){
		return this.channel;
	}
	
	public TextComponent getServerMessage(){
		TextComponent msg = new TextComponent("");
		
		// Server variables
		String serverPrefix = WoMConfig.getServerPrefix(serverName);
		String serverColor = WoMConfig.getServerPrefixColor(serverName);
		ChatColor serverChatColor = ChatColor.DARK_PURPLE;
		try{
			serverChatColor = ChatColor.valueOf(serverColor);
		} catch (IllegalArgumentException | NullPointerException e){
			serverChatColor = ChatColor.DARK_PURPLE;
		}
		TextComponent serverTag = new TextComponent("<"+serverPrefix+">");
			serverTag.setColor(serverChatColor);
		
		// Channel variables
		String channelName = this.channel.getName();
		Boolean displayChannel = this.channel.displayChannel;
		ChatColor channelNameChatColor = this.channel.nameColor;
		TextComponent channel = new TextComponent();
		
		if(displayChannel){
			channel.addExtra("["+channelName+"]");
			channel.setColor(channelNameChatColor);
		}
		
		// Player variables
		TextComponent player = new TextComponent();
		String playerName;
		ChatColor playerNameChatColor = ChatColor.BLUE;
		
		if(this.player != null){
			playerName = this.player.getDisplayName();
			WoMChat.formatName(this.player.getUniqueId(), this.player.getDisplayName(), player);
		}else if(this.uuid != null && this.playerName != null) {
            WoMChat.formatName(UUID.fromString(this.uuid), this.playerName, player);
        }else{
		    playerName = this.playerName;
		    playerNameChatColor = this.playerNameColor;

		    player.addExtra(playerName);
		    player.setColor(playerNameChatColor);
        }

		// text colored to channel specifications
        TextComponent textMessage = new TextComponent(this.messageText);
		textMessage.setColor(this.channel.messageColor);

        // get prefix for user from LuckPerms
        /*LuckPermsApi api = LuckPerms.getApi();
        User user = api.getUser(this.playerName);
        String prefix = null;
        String suffix = null;

        if(this.uuid != null){
            if(!api.isUserLoaded(UUID.fromString(Util.insertDashUUID(this.uuid)))){
                try {
                    user = api.getUserManager().loadUser(UUID.fromString(Util.insertDashUUID(this.uuid))).get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }

        if(user != null){
            prefix = user.getCachedData().getMetaData(api.getContextsForPlayer(this.player)).getPrefix();
            suffix = user.getCachedData().getMetaData(api.getContextsForPlayer(this.player)).getSuffix();
        }*/

        msg.addExtra(serverTag);
        msg.addExtra(channel);

        /*if(prefix != null){
            prefix = ChatColor.translateAlternateColorCodes('&', prefix);
            msg.addExtra(prefix);
        }*/

		msg.addExtra(player);

        /*if(suffix != null){
            suffix = ChatColor.translateAlternateColorCodes('&', suffix);
            msg.addExtra(suffix);
        }*/

		msg.addExtra(":");
		msg.addExtra(textMessage);
		
		return msg;
	}
	
}
