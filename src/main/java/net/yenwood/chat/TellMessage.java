package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.WoMChat;
import net.yenwood.jda.JDABot;
import net.yenwood.util.Util;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TellMessage {
	//private ProxiedPlayer recipient;
	//private ProxiedPlayer sender;

    private String message;

    private String recipientName;
    private UUID recipientUuid;
    private String senderName;
    private UUID senderUuid;

	private TextComponent messageText;

    public TellMessage(ProxiedPlayer to, ProxiedPlayer from, String message){
        //this.recipient = to;
        //this.sender = from;

        this.recipientName = to.getDisplayName();
        this.recipientUuid = to.getUniqueId();
        this.senderName = from.getDisplayName();
        this.senderUuid = from.getUniqueId();

        this.message = message;

        this.formatText();
    }

    public TellMessage(String toUuid, String toName, String fromUuid, String fromName, String message){
        //this.recipient = to;
        //this.sender = from;

        this.recipientName = toName;
        this.recipientUuid = Util.stringToUUID(toUuid);
        this.senderName = fromName;
        this.senderUuid = Util.stringToUUID(fromUuid);

        this.message = message;

        this.formatText();
    }

    public void formatText(){
        String[] words = message.trim().split(" ");
        this.messageText = new TextComponent("");

        // Message text
        for(String word : words){
            // complementary space
            this.messageText.addExtra(" ");

            // URL Check
            Pattern url = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            Pattern mentionPattern = Pattern.compile("<@(.*?)>");

            if(url.matcher(word).find()) {
                TextComponent link = new TextComponent(word);
                link.setClickEvent(new ClickEvent(Action.OPEN_URL, word));
                this.messageText.addExtra(link);
            }else
            if(WoMChat.filter.test(word)){
                this.messageText.addExtra(WoMChat.filter.replace(word));
            }else
            if(mentionPattern.matcher(word).find()){
                Matcher matcher = mentionPattern.matcher(word);
                matcher.matches();
                String userid = matcher.group(1);
                if(JDABot.userExists(userid)){
                    this.messageText.addExtra(JDABot.getUsername(userid));
                }
            }else{
                this.messageText.addExtra(word);
            }
        }
    }
	
	public TextComponent getMessageForRecipient(){
		TextComponent msg = new TextComponent("");
		
		TextComponent startBracket = new TextComponent("[");
			startBracket.setColor(ChatColor.WHITE);
		
		TextComponent senderName = new TextComponent("");
		WoMChat.formatName(this.senderUuid, this.senderName, senderName);
		
		TextComponent indicator = new TextComponent(" -> ");
			indicator.setColor(ChatColor.GRAY);
			
		TextComponent recipientName = new TextComponent("me");
			recipientName.setColor(ChatColor.GRAY);

		TextComponent endBracket = new TextComponent("]");
			endBracket.setColor(ChatColor.WHITE);
			
		msg.addExtra(startBracket);
		msg.addExtra(senderName);
		msg.addExtra(indicator);
		msg.addExtra(recipientName);
		msg.addExtra(endBracket);
		msg.addExtra(this.messageText);
		
		return msg;
	}
	
	public TextComponent getMessageForSender(){
		TextComponent msg = new TextComponent("");
		
		TextComponent startBracket = new TextComponent("[");
			startBracket.setColor(ChatColor.WHITE);
		
		TextComponent senderName = new TextComponent("me");
			senderName.setColor(ChatColor.GRAY);
		
		TextComponent indicator = new TextComponent(" -> ");
			indicator.setColor(ChatColor.GRAY);
			
		TextComponent recipientName = new TextComponent("");
		WoMChat.formatName(this.recipientUuid, this.recipientName, recipientName);

		TextComponent endBracket = new TextComponent("]");
			endBracket.setColor(ChatColor.WHITE);
			
		msg.addExtra(startBracket);
		msg.addExtra(senderName);
		msg.addExtra(indicator);
		msg.addExtra(recipientName);
		msg.addExtra(endBracket);
		msg.addExtra(this.messageText);
		
		return msg;
	}
	
	public TextComponent getMessageForSpy(){
		TextComponent msg = new TextComponent("");
		
		TextComponent startBracket = new TextComponent("[");
			startBracket.setColor(ChatColor.WHITE);
		
		TextComponent senderName = new TextComponent("");
		WoMChat.formatName(this.senderUuid, this.senderName, senderName);
		
		TextComponent indicator = new TextComponent(" -> ");
			indicator.setColor(ChatColor.GRAY);
			
		TextComponent recipientName = new TextComponent("");
		WoMChat.formatName(this.recipientUuid, this.recipientName, recipientName);

		TextComponent endBracket = new TextComponent("]");
			endBracket.setColor(ChatColor.WHITE);
			
		msg.addExtra(startBracket);
		msg.addExtra(senderName);
		msg.addExtra(indicator);
		msg.addExtra(recipientName);
		msg.addExtra(endBracket);
		msg.addExtra(this.messageText);
		
		return msg;
	}
}
