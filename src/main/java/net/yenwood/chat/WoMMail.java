package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.Mail;
import net.yenwood.Mailbox;
import net.yenwood.WoMChat;

public class WoMMail extends Command {
	private WoMChat plugin;

	public WoMMail(WoMChat plugin) {
		super("mail");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		Mailbox mailbox = plugin.mailbox;
		ProxiedPlayer player = (ProxiedPlayer) sender;
		
		// args[0]
		//	send
		//	read
		//	clear
		// args[1]
		//	player
		// args[2]+
		//	message
		
		if(args.length==1){ // read or clear
			ComponentBuilder msg = new ComponentBuilder("").color(ChatColor.GREEN);
			switch(args[0]){
			case "read":
				mailbox.readMail(player);
				break;
			case "clear":
				mailbox.removeAllMail(player);
				msg = new ComponentBuilder("Mail cleared!").color(ChatColor.GREEN);
				sender.sendMessage(msg.create());
				break;
			default:
				msg = new ComponentBuilder("Usage: /mail [read|clear]").color(ChatColor.RED);
				sender.sendMessage(msg.create());
				msg = new ComponentBuilder("Usage: /mail send <player> <message>").color(ChatColor.RED);
				sender.sendMessage(msg.create());
			}
		}else
		if(args.length>=3){
			String recipient = args[1];
			String message = "";
			for(int i = 2; i<args.length; i = i + 1){
				message = message + " " + args[i];
			}
			
			Mail mail = new Mail(mailbox);
			mail.setSender(player.getName());
			mail.setRecipient(recipient.toLowerCase());
			mail.addText(message);
			mail.send();

			ComponentBuilder msg = new ComponentBuilder("Your mail has been sent!").color(ChatColor.GREEN);
			sender.sendMessage(msg.create());
		}else{
			// Not enough arguments
			ComponentBuilder msg = new ComponentBuilder("Usage: /mail [read|clear]").color(ChatColor.RED);
			sender.sendMessage(msg.create());
			msg = new ComponentBuilder("Usage: /mail send <player> <message>").color(ChatColor.RED);
			sender.sendMessage(msg.create());
		}
	}

}
