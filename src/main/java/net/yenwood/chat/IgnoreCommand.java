package net.yenwood.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.WoMChat;

public class IgnoreCommand extends Command {

	private WoMChat plugin;

	public IgnoreCommand(WoMChat plugin) {
		super("ignore");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(args.length==1){
			ProxiedPlayer player = (ProxiedPlayer)sender;
			if(plugin.ignore.isIgnored(player, args[0])){
	    		ComponentBuilder msg = new ComponentBuilder(args[0]+" successfully removed from your ignore list.").color(ChatColor.GREEN);
	    		player.sendMessage(msg.create());
				plugin.ignore.removeIgnore(player, args[0]);
			}else{
	    		ComponentBuilder msg = new ComponentBuilder(args[0]+" successfully added to your ignore list.").color(ChatColor.GREEN);
	    		player.sendMessage(msg.create());
				plugin.ignore.addIgnore(player, args[0]);
			}
		}else{
			ProxiedPlayer player = (ProxiedPlayer)sender;
    		ComponentBuilder msg = new ComponentBuilder("Incorrect amount of arguments.").color(ChatColor.RED)
    				.append("Usage: /ignore <player>").color(ChatColor.RED);
    		player.sendMessage(msg.create());
		}
	}

}
