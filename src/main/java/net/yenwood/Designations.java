package net.yenwood;

import java.io.File;
import java.io.IOException;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Designations {
	private static Configuration configuration;
	private static WoMChat plugin;

	public Designations(WoMChat plugin, Configuration config){
		Designations.configuration = config;
		Designations.plugin = plugin;
	}
	
	public static String get(ProxiedPlayer player){
		String uuid = player.getUniqueId().toString().replace("-", "");
		String designation = configuration.getString("custom."+uuid);
		
		return (designation != null)? designation : "";
	}
	
	public static String get(String path){
		String designation = configuration.getString(path);
		
		return (designation != null)? designation : "";
	}
	
	public static void set(ProxiedPlayer player, String value){
		String uuid = player.getUniqueId().toString().replace("-", "");
		configuration.set(uuid, value);
		WoMChat.designations.put(uuid, value);
	}
	
	public static void set(String path, String value){
		configuration.set(path, value);
		WoMChat.designations.put(path, value);
	}
	
	public void save(){
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(plugin.getDataFolder(), "designations.yml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
