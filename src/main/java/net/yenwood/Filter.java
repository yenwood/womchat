package net.yenwood;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.md_5.bungee.config.Configuration;

public class Filter {
	private Configuration config;
	private List<Pattern> patterns = new ArrayList<Pattern>();
	private List<String> replacements = new ArrayList<String>();
	private Random rand = new Random();
	
	public Filter(Configuration config){
		this.config = config;

		this.load();
	}
	
	public void load(){
		for(String rule : this.config.getStringList("filter.banned")){
			this.patterns.add(Pattern.compile("^" + rule + "$"));
		}
		this.replacements.addAll(this.config.getStringList("filter.replacements"));
	}
	
	public boolean test(String word){
		for(Pattern pattern : this.patterns){
			Matcher matcher = pattern.matcher(word.toLowerCase());
			if(matcher.find()){
				return true;
			}
		}
		return false;
	}
	
	public String replace(String word){
		int num = rand.nextInt(replacements.size());
		return replacements.get(num);
	}
}
