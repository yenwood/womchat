package net.yenwood.database;

import java.io.File;
import java.sql.*;

/**
 * Created by Khorgain on 5/20/2017.
 */
public class Database {
    static String url;

    public Database(File dataFolder){
        url = "jdbc:sqlite:" + dataFolder + "/links.db";

        this.patch();
    }

    public Connection getConnection(){
        try {
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(url);
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet query(String sql){
        Connection conn;
        PreparedStatement ps;
        ResultSet rs = null;

        System.out.println("[WoMChat][Sqlite] SQL: "+sql);

        try{
            conn = this.getConnection();
            ps = conn.prepareStatement(sql);

            rs = ps.executeQuery();
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            /*if(rs != null){
                try{
                    rs.close();
                }catch(SQLException e){}
            }

            if(ps != null){
                try{
                    ps.close();
                }catch(SQLException e){}
            }

            if(conn != null){
                try{
                    conn.close();
                }catch(SQLException e){}
            }*/
        }

        return rs;
    }

    public void update(String sql){
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try{
            conn = this.getConnection();
            ps = conn.prepareStatement(sql);

            ps.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        } finally {

            if(ps != null){
                try{
                    ps.close();
                }catch(SQLException e){}
            }

            if(conn != null){
                try{
                    conn.close();
                }catch(SQLException e){}
            }
        }
    }

    public void patch(){
        this.update("CREATE TABLE IF NOT EXISTS sanctuaryusers (id integer PRIMARY KEY AUTOINCREMENT, uuid text, link text, discord text);");
    }
}
