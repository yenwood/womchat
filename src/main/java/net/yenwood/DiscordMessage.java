package net.yenwood;

public class DiscordMessage {

	private String channelID;
	private String message;

	public DiscordMessage(String channelID, String message) {
		this.channelID = channelID;
		this.message = message;
	}
	
	public String getChannelID(){
		return this.channelID;
	}
	
	public String getMessage(){
		return this.message;
	}

}
