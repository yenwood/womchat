package net.yenwood;

import com.google.common.io.ByteStreams;
import lombok.Getter;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.PermissionData;
import me.lucko.luckperms.api.manager.UserManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.yenwood.chat.*;
import net.yenwood.chatrestriction.ChatRestrictHandler;
import net.yenwood.chatrestriction.RestrictCommand;
import net.yenwood.database.Database;
import net.yenwood.jda.JDABot;
import net.yenwood.pluginchannels.PChannelListener;
import net.yenwood.privatemessaging.WoMReply;
import net.yenwood.privatemessaging.WoMTell;
import net.yenwood.sqsfifo.SQSFIFO;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class WoMChat extends Plugin {
	public static JDABot bot;
	public static Configuration configuration;
	Configuration spiesFile;
	Configuration ignoreFile;
	public String name;
	public String login;
	public String server;
	public int port;
	public String password;
	public Channel global;
	public Channel staff;
	public Channel interpreter;
	public static HashMap<ProxiedPlayer, Channel> playerChannels = new HashMap<ProxiedPlayer, Channel>();
	public static HashMap<String, Channel> channels = new HashMap<String, Channel>();

	public HashMap<String, ProxiedPlayer> online = new HashMap<>();
	public HashMap<String, OnlinePlayerData> onlineAllProxies = new HashMap<>();

	public HashMap<String, String> playerReplies = new HashMap<String, String>();
	public static HashMap<String, String> designations = new HashMap<String, String>();
	public Mailbox mailbox = new Mailbox();
	public Spies spies;
	public static Filter filter;
	public Ignore ignore;
	public WoMTell tell;
	public Configuration descriptionsFile;
	public Descriptions descriptions;
	public Configuration designationsFile;
	public Designations designationsC;
    static public @Getter Logger log;
	static public boolean apiready;
	static public List<String> bspinputs = new ArrayList<String>();

	static public Database db;

	static public SQSFIFO sqsfifo;

	static public WoMChat instance;
	
    @Override
    public void onEnable() {
        instance = this;

        WoMChat.log = getLogger();
    	loadConfig();
        WoMConfig womConfig = new WoMConfig(configuration);

        // test fifo stuff
        sqsfifo = new SQSFIFO(this);

        ChatRestrictHandler chatRestrictHandler = new ChatRestrictHandler(this);

    	// database
        db = new Database(getDataFolder());
    	
    	// Start the bot
    	this.startBot();
    	
    	// Make the filter, initalize it with the config
    	WoMChat.filter = new Filter(WoMChat.configuration);

    	// Make the spies list.
    	this.spies = new Spies(this, this.spiesFile);
    	
    	// Make the ignore list.
    	this.ignore = new Ignore(this, this.ignoreFile);
    	
    	// Make the descriptions list.
    	this.descriptions = new Descriptions(this, this.descriptionsFile);
    	
    	// Make the designations.
    	this.designationsC = new Designations(this, this.designationsFile);
    	
    	// Make the Global, Staff and Interpreter channels
    	this.global = new Channel("global", true, true, true, this);
    	this.staff = new Channel("staff", true, true, false, this);
    	this.interpreter = new Channel("interpreter", true, true, false, this);
    	this.staff.setMessageColor(ChatColor.LIGHT_PURPLE);
    	this.staff.setNameColor(ChatColor.LIGHT_PURPLE);
    	
    	WoMChat.channels.put("global", this.global);
    	WoMChat.channels.put("staff", this.staff);
    	
    	// Register listeners
        getProxy().getPluginManager().registerListener(this, new WoMChatListener(this, bot));
        
        String[] tellAlias = new String[8];
        tellAlias[0] = "t";
        tellAlias[1] = "w";
        tellAlias[2] = "whisper";
        tellAlias[3] = "m";
        tellAlias[4] = "msg";
        tellAlias[5] = "message";
        tellAlias[6] = "pm";
        tellAlias[7] = "privatemessage";
        
        // Register commands
        getProxy().getPluginManager().registerCommand(this, new WoMChatCommands(this));
        getProxy().getPluginManager().registerCommand(this, new WoMChatChannels(this));
        this.tell = new WoMTell(this, tellAlias);
        getProxy().getPluginManager().registerCommand(this, this.tell);
        getProxy().getPluginManager().registerCommand(this, new WoMReply(this));
        getProxy().getPluginManager().registerCommand(this, new WoMChatSpy(this));
        //getProxy().getPluginManager().registerCommand(this, new WoMMail(this));
        getProxy().getPluginManager().registerCommand(this, new IgnoreCommand(this));
        getProxy().getPluginManager().registerCommand(this, new RestrictCommand(this));
        
        // Channels
		ProxyServer.getInstance().getPluginManager().registerListener(this, new PChannelListener(this));
		ProxyServer.getInstance().registerChannel("Return");
    }
    
    @Override
    public void onDisable() {    	
    	spies.save();
    	ignore.save();
    	descriptions.save();
    	designationsC.save();
    	JDABot.stop();

    	sqsfifo.DeleteQueue();
    }

    /**
     * Loads the various configs of WoMChat
     */
	public void loadConfig() {
        // Make the config file if it doesn't exist
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        
        // Config...
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
        // Get the config file
        try {
			WoMChat.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
		} catch (IOException e) {
			throw new RuntimeException("Unable to get config file.");
		}
        
        this.name = configuration.getString("name");
        this.login = configuration.getString("login");
        this.server = configuration.getString("server");
        this.port = configuration.getInt("port");
        this.password = configuration.getString("password");
        
		new LocalConfiguration(configuration);

        // Spies...
        File spiesFile = new File(getDataFolder(), "spies.yml");
        if (!spiesFile.exists()) {
            try {
                spiesFile.createNewFile();
                try (InputStream is = getResourceAsStream("spies.yml");
                     OutputStream os = new FileOutputStream(spiesFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create spies file", e);
            }
        }
        try {
			this.spiesFile = ConfigurationProvider.getProvider(YamlConfiguration.class).load(spiesFile);
		} catch (IOException e) {
			throw new RuntimeException("Unable to get spies file.");
		}
        
        // Ignores...
        File ignoreFile = new File(getDataFolder(), "ignores.yml");
        if (!ignoreFile.exists()) {
            try {
                ignoreFile.createNewFile();
                try (InputStream is = getResourceAsStream("ignores.yml");
                     OutputStream os = new FileOutputStream(ignoreFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create ignore file", e);
            }
        }
        try {
			this.ignoreFile = ConfigurationProvider.getProvider(YamlConfiguration.class).load(ignoreFile);
		} catch (IOException e) {
			throw new RuntimeException("Unable to get ignore file.");
		}
        
        // Descriptions...
        File descriptionsFile = new File(getDataFolder(), "descriptions.yml");
        if (!descriptionsFile.exists()) {
            try {
            	descriptionsFile.createNewFile();
                try (InputStream is = getResourceAsStream("descriptions.yml");
                     OutputStream os = new FileOutputStream(descriptionsFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create descriptions file", e);
            }
        }
        try {
			this.descriptionsFile = ConfigurationProvider.getProvider(YamlConfiguration.class).load(descriptionsFile);
		} catch (IOException e) {
			throw new RuntimeException("Unable to get ignore file.");
		}
        
        // Designations...
        File designationsFile = new File(getDataFolder(), "designations.yml");
        if (!designationsFile.exists()) {
            try {
            	designationsFile.createNewFile();
                try (InputStream is = getResourceAsStream("designations.yml");
                     OutputStream os = new FileOutputStream(designationsFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create designations file", e);
            }
        }
        try {
			this.designationsFile = ConfigurationProvider.getProvider(YamlConfiguration.class).load(designationsFile);
		} catch (IOException e) {
			throw new RuntimeException("Unable to get designations file.");
		}
	}
	
	/**
	 * Returns true if player is in the online list, else false
	 * 
	 * @param name	the name of the player
	 * @return True if the player is online, otherwise false
	 */
	public boolean isOnline(String name){
		name = name.toLowerCase();
        return online.containsKey(name);
    }
	
	/**
	 * Gets a player from the online list
	 * 
	 * @param name	the name of the player
	 * @return Player with the name or null
	 */
	public ProxiedPlayer getPlayer(String name){
		name = name.toLowerCase();
		if(online.containsKey(name)){
			return online.get(name);
		}
		return null;
	}

	/**
	 * Turns username into a properly colored username with a hover event
	 * 
	 * @param puuid	The player to be put into the TextComponent
	 * @param username	The TextComponent to be formatted
	 */
	public static void formatName(UUID puuid, String playerName, TextComponent username) {
        String uuid = puuid.toString().replace("-", "");
        String rank;
        String rankDesc;
        ComponentBuilder hovermsg = new ComponentBuilder("");
        TextComponent nameplate = new TextComponent();

        LuckPermsApi api = LuckPerms.getApi();
        UserManager userManager = api.getUserManager();
        Contexts context = api.getContextManager().getStaticContexts();
        User user = userManager.getLoadedUsers().iterator().next(); // this is rly messy and unnecessary

        if (userManager.isLoaded(puuid)) {
            user = userManager.getUser(puuid);
        }else{
            try {
                user = userManager.loadUser(puuid).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        PermissionData perm = user.getCachedData().getPermissionData(context);

        //perm.getPermissionValue("womchat.staff").asBoolean();

        username.addExtra(playerName);

		ChatColor namecolor = ChatColor.BLUE;

        for (ChatColor chatcolor : ChatColor.values()) {
            String colorname = chatcolor.getName();
            if(perm.getPermissionValue("womchat.namecolor."+colorname.toLowerCase()).asBoolean()){
                namecolor = chatcolor;
            }
        }

		/* Name
		if(perm.getPermissionValue("womchat.staff").asBoolean()){
			namecolor = ChatColor.DARK_PURPLE;
		}else
		if(perm.getPermissionValue("womchat.trusted").asBoolean()){
			namecolor = ChatColor.AQUA;
		}*/

		/* hover message */

        hovermsg.append(playerName).color(namecolor);
		
		// Linebreak cuz why not
		hovermsg.append("\n");
		
		// Hover
		String designation = WoMChat.designations.get(uuid);

		if(designation == null){
            hovermsg.append("Undesignated").color(ChatColor.DARK_RED);
            hovermsg.append("\n");
        }else if(designation.startsWith("D")){
            hovermsg.append("Designation "+designation);
            hovermsg.append("\n");
        }else if(designation.equalsIgnoreCase("STAFF")||designation.equalsIgnoreCase("T2")||designation.equalsIgnoreCase("T1")){
			// Nothing
		}else{
			hovermsg.append("Undesignated").color(ChatColor.DARK_GRAY);
			hovermsg.append("\n");
		}
		
		// Rank
		if(perm.getPermissionValue("womchat.staff").asBoolean()){
			username.setColor(namecolor);
			
			hovermsg.append("Staff").color(ChatColor.DARK_PURPLE);
			hovermsg.append("\n");
			
			rank = "staff";
		}else
		if(perm.getPermissionValue("womchat.trusted").asBoolean()){
			username.setColor(namecolor);
			
			hovermsg.append("Trusted").color(ChatColor.AQUA);
			hovermsg.append("\n");
			
			rank = "trusted";
		}else{
			username.setColor(namecolor);
			
			hovermsg.append("Member").color(ChatColor.BLUE);
			hovermsg.append("\n");
			
			rank = "member";
		}
		
		// Description priority: user | designation | rank
		/*if(Descriptions.get(player)!=null){
			rankDesc = Descriptions.get(player);
		}else
		if(!(designation.equalsIgnoreCase("STAFF")||designation.equalsIgnoreCase("T2")||designation.equalsIgnoreCase("T1"))){
			rankDesc = Descriptions.get("designations."+designation.toLowerCase());
		}else{
			rankDesc = Descriptions.get("ranks."+rank);
		}
		
		hovermsg.append(rankDesc).color(ChatColor.WHITE);*/
		
		username.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hovermsg.create()));
	}

	/**
	 * Removes a channel from the channel list
	 * 
	 * @param channel	The channel object to remove.
	 */
	public void deleteChannel (Channel channel) {
		if(channels.containsValue(channel)){
			channels.remove(channel.getName());
			channel = null;
		}
	}

	/**
	 * Checks if the channel is listed
	 * 
	 * @param name	Name of the channel
	 * @return	True if the channel exists, false if not.
	 */
	public static boolean channelExists(String name) {
        return channels.containsKey(name);
	}
	
	/**
	 * Returns the channel a player is in
	 * 
	 * @param player	The player object
	 * @return	The channel the player is in, null if the player is not online or in a channel
	 */
	public static Channel getChannel(ProxiedPlayer player){
		if(playerChannels.containsKey(player)){
			return playerChannels.get(player);
		}else{
			return null;
		}
	}
	
	
	/**
	 * Get a channel by its name
	 * 
	 * @param name	Name of the channel
	 * @return	Returns channel, null if it doesn't exist.
	 */
	public static Channel getChannel(String name){
		if(channels.containsKey(name)){
			return channels.get(name);
		}else{
			return null;
		}
	}

	/**
	 * Starts the Discord chatbot.
	 */
	public void startBot() {
		WoMChat.bot = new JDABot(this);
	}
	
	public void stopBot() {
		
	}
	
	/**
	 * Dispatches a command to the proxy.
	 * 
	 * @param cmd	The command and its arguments.
	 */
	public void proxyCmd(String cmd){
        System.out.println("Running command: "+cmd);
		if(getProxy().getPluginManager().dispatchCommand(getProxy().getConsole(), cmd)){
			System.out.println("Command success.");
		}else{
            System.out.println("Command failed.");
		}
	}
	
	/**
	 * Doesn't work.
	 * 
	 * @param servername	Server to send a command to.
	 * @param cmd	The command and its arguments.
	 */
	public void serverCmd(String servername, String cmd){
		getProxy().getServerInfo(servername);
		if(getProxy().getPluginManager().dispatchCommand(getProxy().getConsole(), cmd)){

		}else{

		}
	}

	/**
	 * Adds a channel to the WoMChat list
	 *
	 * @param nChannel	Channel object to add.
	 */
	public static void addChannel(Channel nChannel) {
		//	channels.add(nChannel);
	}

	/**
	 * Adds player UUID to ignore the next message of.
	 *
	 * @param input	Player UUID
	 */
	public void BSPInput(String input) {
		WoMChat.bspinputs.add(input);
	}

}