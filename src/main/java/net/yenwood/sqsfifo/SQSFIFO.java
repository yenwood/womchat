package net.yenwood.sqsfifo;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.handlers.AsyncHandler;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.Channel;
import net.yenwood.LocalConfiguration;
import net.yenwood.WoMChat;
import net.yenwood.chat.TellMessage;
import net.yenwood.util.Util;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class SQSFIFO {
    public AmazonSQSAsync mainRegion;
    public String queueName;
    public String queueUrl;

    public WoMChat plugin;

    public List<String> inputQueues = new ArrayList<>();

    public SQSFIFO(WoMChat plugin) {
        this.plugin = plugin;
        /*
         * Create a new instance of the builder with all defaults (credentials
         * and region) set automatically. For more information, see
         * Creating Service Clients in the AWS SDK for Java Developer Guide.
         */

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(LocalConfiguration.getAccessKey(), LocalConfiguration.getSecretKey());

        mainRegion = AmazonSQSAsyncClientBuilder.standard()
               .withRegion(Regions.valueOf(LocalConfiguration.getRegion()))
               .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
               .build();

        try {
            final Map<String, String> attributes = new HashMap<>();

            // A FIFO queue must have the FifoQueue attribute set to true.
            attributes.put("FifoQueue", "true");

            /*
             * If the user doesn't provide a MessageDeduplicationId, generate a
             * MessageDeduplicationId based on the content.
             */
            attributes.put("ContentBasedDeduplication", "true");

            // long polling
            attributes.put("ReceiveMessageWaitTimeSeconds", "20");

            if(LocalConfiguration.isAutoManageQueue()){
                queueName = Util.getNow()+""+LocalConfiguration.getQueueName();
            }else{
                queueName = LocalConfiguration.getQueueName();
            }


            // The FIFO queue name must end with the .fifo suffix.
            final CreateQueueRequest createQueueRequest =
                    new CreateQueueRequest(queueName+"Input.fifo")
                            .withAttributes(attributes);

            if(LocalConfiguration.isAutoManageQueue()){
                queueUrl = mainRegion.createQueue(createQueueRequest).getQueueUrl();
            }else{
                queueUrl = mainRegion.getQueueUrl(LocalConfiguration.getQueueName()).getQueueUrl();
            }

            findInputQueues();

            // Receive messages.
            receiveMessages();
        } catch (final AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means " +
                    "your request made it to Amazon SQS, but was " +
                    "rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (final AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means " +
                    "the client encountered a serious internal problem while " +
                    "trying to communicate with Amazon SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }

        // schedule a repeating task to grab queues
        ProxyServer.getInstance().getScheduler().schedule(this.plugin, new Runnable() {
            @Override
            public void run() {
                findInputQueues();
            }
        }, LocalConfiguration.getQueueSearchInterval(), LocalConfiguration.getQueueSearchInterval(), TimeUnit.SECONDS);
    }

    public void findInputQueues(){
        // empty the list so it doesnt have old queues in it
        this.inputQueues.clear();

        // List all queues.
        for (final String queueUrl : mainRegion.listQueues().getQueueUrls()) {
            if(!queueUrl.equals(this.queueUrl) && queueUrl.endsWith("Input.fifo")){
                this.inputQueues.add(queueUrl);
            }
        }
    }

    public void receiveMessages(){
        ReceiveMessageRequest rq = new ReceiveMessageRequest().withQueueUrl(this.queueUrl).withWaitTimeSeconds(20);

        this.mainRegion.receiveMessageAsync(rq, new AsyncHandler<ReceiveMessageRequest, ReceiveMessageResult>() {
            @Override
            public void onError(Exception e) {
                receiveMessages();
            }

            @Override
            public void onSuccess(ReceiveMessageRequest request, ReceiveMessageResult receiveMessageResult) {
                processMessages(receiveMessageResult);
                receiveMessages();
            }
        });
    }

    public void processMessages(ReceiveMessageResult receiveMessageResult){
        List<Message> messages = receiveMessageResult.getMessages();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        for(Message messageRaw : messages){
            SQSMessage message = gson.fromJson(messageRaw.getBody(), SQSMessage.class);

            if(message.type.equalsIgnoreCase("privatemessage")){
                String[] args = message.content.split(" ");

                if(plugin.isOnline(message.target)){
                    WoMChat.getLog().info("Target player "+message.target+" is online");
                    ProxiedPlayer to = plugin.getPlayer(message.target);
                    String toName = to.getName();
                    String senderName = message.username;

                    String msgContent = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

                    TellMessage msg = new TellMessage(to.getUniqueId().toString(), to.getDisplayName(), message.uuid, message.username, msgContent);

                    to.sendMessage(msg.getMessageForRecipient());
                    plugin.playerReplies.put(toName, senderName);

                    List<String> involved = new ArrayList<String>();
                    involved.add(senderName);
                    involved.add(toName);
                    plugin.spies.sendMessage(msg.getMessageForSpy(), involved);
                }else{
                    WoMChat.getLog().info("Target player "+message.target+" is not online");
                }
            }else if(message.type.equalsIgnoreCase("chatmessage")){
                Channel channel = WoMChat.getChannel(message.channel);
                if(channel == null){
                    Channel nChannel = new Channel(message.channel, plugin);
                    WoMChat.addChannel(nChannel);
                    channel = nChannel;
                }
                channel.sendMessage(message.uuid, message.username, message.source, message.content);
            }else{
                WoMChat.getLog().info("Unknown SQS/FIFO message received:");
                WoMChat.getLog().info(messageRaw.getBody());
            }

            String messageReceiptHandle = messageRaw.getReceiptHandle();
            mainRegion.deleteMessage(queueUrl, messageReceiptHandle);
        }
    }

    public void sendMessage(String content){
        // Send a message to all input queues

        final List<String> inputQueues = this.inputQueues;
        final AmazonSQSAsync mainRegion = this.mainRegion;

        ProxyServer.getInstance().getScheduler().runAsync(WoMChat.instance, new Runnable() {
            @Override
            public void run() {

                for(String queueUrl : inputQueues){
                    final SendMessageRequest sendMessageRequest =
                            new SendMessageRequest(queueUrl, content);

                    /*
                     * When you send messages to a FIFO queue, you must provide a
                     * non-empty MessageGroupId.
                     */
                    sendMessageRequest.setMessageGroupId("messageGroup1");

                    // Uncomment the following to provide the MessageDeduplicationId
                    //sendMessageRequest.setMessageDeduplicationId("1");
                    final SendMessageResult sendMessageResult = mainRegion.sendMessage(sendMessageRequest);
                }
            }
        });
    }

    public void sendMessage(SQSMessage sqsMessage){
        Gson gson = new GsonBuilder().create();
        sendMessage(gson.toJson(sqsMessage));
    }

    public void DeleteQueue(){
        if(LocalConfiguration.isAutoManageQueue()){
            // Delete the queue.
            System.out.println("Deleting the queue.\n");
            mainRegion.deleteQueue(new DeleteQueueRequest(queueUrl));
        }
    }
}
