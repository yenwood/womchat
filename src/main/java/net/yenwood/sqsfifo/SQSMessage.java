package net.yenwood.sqsfifo;

public class SQSMessage {
    public String username = "Herobrine";
    public String uuid = "123";

    public String type = "chatmessage"; // "userlist", "command", "chatmessage" or "privatemessage"

    // chatmessage or privatemessage
    public String content = "Aaahhh!!!";

    // chatmessage only
    public String channel = "global";

    // privatemessage only
    public String target = "Yenwood";

    public String source = "discord"; // server it came from, or discord
    public String timestamp = "12345676151"; // for deduplication
}
