package net.yenwood.pluginchannels;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.yenwood.WoMChat;

import java.io.*;

public class PChannelListener implements Listener {
	private WoMChat plugin;
	
	public PChannelListener(WoMChat plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPluginMessage(PluginMessageEvent e){
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(e.getData()));

        if (e.getTag().equalsIgnoreCase("BungeeCord")) {
        	try{
        		String subchannel = dis.readUTF();
        		
        		if(subchannel.equalsIgnoreCase("BossShopPro")){
        			String type = dis.readUTF();

                    if(type.equalsIgnoreCase("PlayerInput")){
            			String playerUUID = dis.readUTF();
                        String inputType = dis.readUTF();
                        if(inputType.equalsIgnoreCase("start")){
                    		this.plugin.BSPInput(playerUUID);
                        }
                        if(inputType.equalsIgnoreCase("end")){
                        	WoMChat.bspinputs.remove(playerUUID);
                        }
                    }
        		}else
            	if(subchannel.equalsIgnoreCase("command")){
        			String input = dis.readUTF();
            		this.plugin.proxyCmd(input);
            	}
        	} catch (IOException e1) {            
                e1.printStackTrace();            
            }
        }
    }
	
	public static void sendToServer(String channel, String message, ServerInfo server){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(stream);
		
		try{
			out.writeUTF(channel);
			out.writeUTF(message);
		}catch(IOException e){
			e.printStackTrace();
		}
		
		server.sendData("Return", stream.toByteArray());
	}
}
