package net.yenwood;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Spies {
	private WoMChat plugin;
	private List<String> spyList;
	private Configuration configuration;
	
	public Spies(WoMChat plugin, Configuration config){
		this.plugin = plugin;
		this.configuration = config;
		
		this.spyList = this.configuration.getStringList("spies");
	}
	
	public void add(ProxiedPlayer player){
		String name = player.getName();
		if(!this.spyList.contains(name)){
			this.spyList.add(name);
		}
	}
	
	public void remove(ProxiedPlayer player){
		if(this.spyList.contains(player.getName())){
			this.spyList.remove(player.getName());
		}
	}
	
	public boolean contains(ProxiedPlayer player){
        return this.spyList.contains(player.getName());
    }
	
	public void save(){
		this.configuration.set("spies", this.spyList);
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.configuration, new File(plugin.getDataFolder(), "spies.yml"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void sendMessage(TextComponent finalMessage, Channel origin, List<String> involved) {
		for(String name : this.spyList){
			if(plugin.isOnline(name)&&!involved.contains(name)){
				ProxiedPlayer spy = plugin.getPlayer(name);
				if(!origin.isListener(spy)){
					spy.sendMessage(finalMessage);
				}
			}
		}
	}

	public void sendMessage(TextComponent finalMessagetoSpies, List<String> involved) {
		for(String name : this.spyList){
			if(plugin.isOnline(name)&&!involved.contains(name)){
				ProxiedPlayer spy = plugin.getPlayer(name);
				spy.sendMessage(finalMessagetoSpies);
			}
		}
	}
}
