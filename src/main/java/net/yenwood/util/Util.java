package net.yenwood.util;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Utility class containing assorted methods that do not fit other categories
 * @author bitWolfy
 *
 */
public class Util {
    
    private Util() { }

    public static String insertDashUUID(String uuid) {
        StringBuffer sb = new StringBuffer(uuid.replace("-", " "));
        sb.insert(8, "-");

        sb = new StringBuffer(sb.toString());
        sb.insert(13, "-");

        sb = new StringBuffer(sb.toString());
        sb.insert(18, "-");

        sb = new StringBuffer(sb.toString());
        sb.insert(23, "-");

        return sb.toString();
    }

    public static UUID stringToUUID(String uuid) {
        return UUID.fromString(uuid);
    }
    
    public static String parseString(String str) {
        if(str == null) return "NULL";

        str = str.replace("\u00A7", "&");
        str = str.replace("'", "''");
        
        return str;
    }
    
    /**
     * Parses the string, replacing ampersand-codes with CraftBukkit color codes
     * @param str String to be parsed
     * @return Parsed string
     */
    public static String parseChatColors(String str) {
    	return str;
        //if(str == null) return "";
        //for(ChatColor color : ChatColor.values()) str = str.replaceAll("&" + color.getChar(), color + "");
        //return str;
    }
    
    /**
     * Returns the current time in seconds.
     * @return Current time
     */
    public static long getTimestamp() {
        Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
        return timestamp.getTime() / 1000;
    }
    
    /**
     * Changes a timestamp into a user-friendly form
     * @param timestamp Timestamp to parse
     * @return User-friendly output
     */
    public static String parseTimestamp(long timestamp) {
        String result = "";
        DecimalFormat f = new DecimalFormat ("00");
        int days = (int) (timestamp / 86400);
        int hours = (int) ((timestamp - (days * 86400)) / 3600);
        int minutes = (int) ((timestamp - (days * 86400) - (hours * 3600)) / 60);
        int seconds = (int) (timestamp - (days * 86400) - (hours * 3600) - (minutes * 60));
        if(days != 0) result += days + " days, ";
        result += f.format(hours) + ":" + f.format(minutes) + ":" + f.format(seconds);
        return result;
    }
    
    public static String milliToReadable(long millis){
		String readable;
		
		if(TimeUnit.MILLISECONDS.toMinutes(millis) > 0){
			readable = String.format("%d minutes and %d seconds", 
	    		    TimeUnit.MILLISECONDS.toMinutes(millis),
	    		    TimeUnit.MILLISECONDS.toSeconds(millis) - 
	    		    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
	    		);
		}else{
			readable = String.format("%d seconds", 
	    		    TimeUnit.MILLISECONDS.toMinutes(millis),
	    		    TimeUnit.MILLISECONDS.toSeconds(millis) - 
	    		    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
	    		);
		}
		
		return readable;
	}
    public static String milliToReadable(long start, long end){
		return milliToReadable(end - start);
	}
    
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

	public static long getNow() {
		return new java.util.Date().getTime();
	}

	public static void sendMessage(ProxiedPlayer player, ChatColor color, String message) {
		TextComponent msg = new TextComponent(message);
		msg.setColor(color);
		
		player.sendMessage(msg);
	}
	public static void sendMessage(ProxiedPlayer player, String message) {
		sendMessage(player, ChatColor.WHITE, message);
	}
    
}
