package net.yenwood.privatemessaging;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.WoMChat;
import net.yenwood.chatrestriction.ChatRestrictHandler;

public class WoMReply extends Command {
	private WoMChat plugin;

	public WoMReply(WoMChat plugin){
		super("r", "womchat.sendmessages", "reply");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sent, String[] args) {
		// args[0]+ message
		ProxiedPlayer sender = (ProxiedPlayer)sent;
		
		if(ChatRestrictHandler.isRestricted(sender)){
			if(ChatRestrictHandler.getMessages(sender) <= 0){
				TextComponent message1 = new TextComponent("");
				message1.addExtra("You've run out of messages!");
				message1.setColor(ChatColor.RED);
				sender.sendMessage(message1);
				return;
			}else{
				ChatRestrictHandler.getChatRestrict(sender).decrementMessages();
			}
		}
		
		String toName = plugin.playerReplies.get(sender.getName());
		if(toName.startsWith("!IRC:")){
			toName = toName.substring(5);
			String message = "";
			for(int i = 0; i<args.length; i++){
				message = message+" "+args[i];
			}
			
			ComponentBuilder msg = new ComponentBuilder("");
			msg.append("[").color(ChatColor.GOLD); // Bracket
			msg.append("me").color(ChatColor.GRAY);
			msg.append(" -> ").color(ChatColor.WHITE); // ->
			msg.append(toName).color(ChatColor.DARK_PURPLE); // To
			msg.append("]").color(ChatColor.GOLD); // Bracket
			msg.append(message).color(ChatColor.WHITE); // Text
			sender.sendMessage(msg.create());
		}else
		if(plugin.isOnline(toName)){
			ProxiedPlayer to = plugin.getPlayer(toName);
			String[] arg = new String[args.length + 1];
			arg[0] = to.getName();
            System.arraycopy(args, 0, arg, 1, args.length);
			plugin.tell.execute(sent, arg);
		}else{
			ComponentBuilder msg = new ComponentBuilder("No one to reply to.").color(ChatColor.RED);
			sender.sendMessage(msg.create());
		}
	}

}
