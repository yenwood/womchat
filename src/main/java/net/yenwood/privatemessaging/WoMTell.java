package net.yenwood.privatemessaging;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.yenwood.WoMChat;
import net.yenwood.chat.TellMessage;
import net.yenwood.chatrestriction.ChatRestrictHandler;
import net.yenwood.sqsfifo.SQSMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static net.yenwood.WoMChat.sqsfifo;

public class WoMTell extends Command {

	private WoMChat plugin;
	
	public WoMTell(WoMChat plugin, String[] aliases) {
		super("tell", "womchat.sendmessages", aliases);
		this.plugin = plugin;
	}

	@Override
    public void execute(CommandSender sent, String[] args) {
        // args[0] person to send to
        // args[1]+ message
        ProxiedPlayer sender = (ProxiedPlayer)sent;

        if(ChatRestrictHandler.isRestricted(sender)){
            if(ChatRestrictHandler.getMessages(sender) <= 0){
                TextComponent message1 = new TextComponent("");
                message1.addExtra("You've run out of messages!");
                message1.setColor(ChatColor.RED);
                sender.sendMessage(message1);
                return;
            }else{
                ChatRestrictHandler.getChatRestrict(sender).decrementMessages();
            }
        }

        String message = "";
        for(int i = 1; i<args.length; i++){
            message = message + " " + args[i];
        }

        //LuckPermsApi api = LuckPerms.getApi();
        //UserManager userManager = api.getUserManager();

        // may or may not be online
        //User user = userManager.getUser(args[0]);

        if(ProxyServer.getInstance().getPlayer(args[0]) != null){
            // player is online
            ProxiedPlayer to = plugin.getPlayer(args[0]);
            String toName = to.getName();
            String senderName = sender.getName();

            TellMessage msg = new TellMessage(to, sender, message);

            sender.sendMessage(msg.getMessageForSender());
            to.sendMessage(msg.getMessageForRecipient());
            plugin.playerReplies.put(toName, senderName);

            List<String> involved = new ArrayList<String>();
            involved.add(senderName);
            involved.add(toName);
            plugin.spies.sendMessage(msg.getMessageForSpy(), involved);
        }else{
            SQSMessage sqsmsg = new SQSMessage();

            sqsmsg.username = sender.getDisplayName();
            sqsmsg.uuid = sender.getUniqueId().toString();
            sqsmsg.type = "privatemessage";
            sqsmsg.source = sender.getServer().getInfo().getName();
            sqsmsg.content = message;
            sqsmsg.target = args[0];

            sqsfifo.sendMessage(sqsmsg);
        }
    }

	/*@Override
	public void execute(CommandSender sent, String[] args) {
		// args[0] person to send to
		// args[1]+ message
		ProxiedPlayer sender = (ProxiedPlayer)sent;
		
		if(ChatRestrictHandler.isRestricted(sender)){
			if(ChatRestrictHandler.getMessages(sender) <= 0){
				TextComponent message1 = new TextComponent("");
				message1.addExtra("You've run out of messages!");
				message1.setColor(ChatColor.RED);
				sender.sendMessage(message1);
				return;
			}else{
				ChatRestrictHandler.getChatRestrict(sender).decrementMessages();
			}
		}
		
		String message = "";
		for(int i = 1; i<args.length; i++){
			message = message + " " + args[i];
		}

        LuckPermsApi api = LuckPerms.getApi();
        UserManager userManager = api.getUserManager();

        // may or may not be online
        User user = userManager.getUser(args[0]);

		if(plugin.isOnline(args[0])){
		    // user is online on this proxy, ez
			ProxiedPlayer to = plugin.getPlayer(args[0]);
			String toName = to.getName();
			String senderName = sender.getName();

			TellMessage msg = new TellMessage(to, sender, message);

			sender.sendMessage(msg.getMessageForSender());
			to.sendMessage(msg.getMessageForRecipient());
			plugin.playerReplies.put(toName, senderName);
			
			List<String> involved = new ArrayList<String>();
			involved.add(senderName);
			involved.add(toName);
			plugin.spies.sendMessage(msg.getMessageForSpy(), involved);
		}else if(user != null) {
            // user is NOT online on this proxy, but luckperms data is loaded so they MAY be online somewhere
        }else{
		    ComponentBuilder msg = new ComponentBuilder("That player is not online.").color(ChatColor.RED);
		    sender.sendMessage(msg.create());
        }
	}*/

	public void sendIRC(ProxiedPlayer to, String sender, String message) {
		String toName = to.getName();

		TextComponent fromName = new TextComponent(sender);
		TextComponent toName2 = new TextComponent("");
		TextComponent text = new TextComponent("");
		TextComponent finalMessagetoRecipient = new TextComponent("");
		TextComponent finalMessagetoSpies = new TextComponent("");

		// Process player names
		fromName.setColor(ChatColor.DARK_PURPLE);
		WoMChat.formatName(to.getUniqueId(), to.getDisplayName(), toName2);
		
		// Process text
		for(String word : message.split(" ")){
            text.addExtra(" ");
            // URL Check
            Pattern url = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
            
            if (url.matcher(word).find()) {
            	TextComponent link = new TextComponent(word);
            	link.setClickEvent(new ClickEvent(Action.OPEN_URL, word));
            	
            	text.addExtra(link);
            }else{
            	text.addExtra(word);
            }
        }

		// Process for recipient
		finalMessagetoRecipient.addExtra("[");
		finalMessagetoRecipient.addExtra(fromName);
		finalMessagetoRecipient.addExtra(" -> ");
		finalMessagetoRecipient.addExtra("me");
		finalMessagetoRecipient.addExtra("]");
		finalMessagetoRecipient.addExtra(text);

		// Process for spies
		finalMessagetoSpies.addExtra("[");
		finalMessagetoSpies.addExtra(fromName);
		finalMessagetoSpies.addExtra(" -> ");
		finalMessagetoSpies.addExtra(toName2);
		finalMessagetoSpies.addExtra("]");
		finalMessagetoSpies.addExtra(text);
		
		to.sendMessage(finalMessagetoRecipient);
		
		List<String> involved = new ArrayList<String>();
		involved.add(toName);
		plugin.spies.sendMessage(finalMessagetoSpies, involved);
	}

}
