package net.yenwood;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.yenwood.chatrestriction.ChatRestrict;
import net.yenwood.chatrestriction.ChatRestrictHandler;
import net.yenwood.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SpamHandler {
	private static HashMap<ProxiedPlayer, List<Long>> messageList = new HashMap<ProxiedPlayer, List<Long>>();
	private static HashMap<ProxiedPlayer, List<Long>> warningList = new HashMap<ProxiedPlayer, List<Long>>();
	
	public SpamHandler(){
		
	}
	
	public static boolean process(ProxiedPlayer player){
		if(messageList.containsKey(player)){
			// Purge old messages/warnings first
			purge(player);

			List<Long> messages = messageList.get(player);
			List<Long> warnings = warningList.get(player);
			
			messageList.get(player).add(Util.getNow());
			
			if(messages.size() > LocalConfiguration.maxMessages){
				// Spamming
				
				if(warnings.size() >= LocalConfiguration.maxMessages){
					// Player has been spamming. Restrict them with defaults.
					ChatRestrict chr = new ChatRestrict()
							.setMessages(2)
							.setExpire(System.currentTimeMillis() + (900000))
							.setInterval(120)
							.setActive(true);
					ChatRestrictHandler.restrict(player, chr);
					
					// Clear out the player from the messages/warnings lists
					messageList.remove(player);
					warningList.remove(player);
					return true;
				}else{
					Util.sendMessage(player, ChatColor.RED, "<WoMChat> You're spamming! Slow down or you'll get chat restricted.");
					warningList.get(player).add(Util.getNow());
				}
			}
		}else{
			messageList.put(player, new ArrayList<Long>());
			warningList.put(player, new ArrayList<Long>());
		}
		return false;
	}
	
	public static void purge(ProxiedPlayer player){
		// Clear out old messages and warnings
		if(messageList.containsKey(player)){
			Long oldestMessage = Util.getNow() - (LocalConfiguration.messageCooldown * 1000);
			Long oldestWarning = Util.getNow() - (LocalConfiguration.messageCooldown * 1000);

			Iterator<Long> messageIter = messageList.get(player).iterator();
			Iterator<Long> warningIter = warningList.get(player).iterator();

			while (messageIter.hasNext()) {
			    Long msg = messageIter.next();

			    if (msg <= oldestMessage)
			    	messageIter.remove();
			}
			
			while (warningIter.hasNext()) {
			    Long warn = warningIter.next();

			    if (warn <= oldestWarning)
			    	warningIter.remove();
			}
		}
	}
}
